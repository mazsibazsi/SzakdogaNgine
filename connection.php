<?php
class Database {

	private static $servername;
	private static $username;
	private static $password;
	private static $dbname;
	private static $conn;

	public function __construct() {
		self::$conn = new mysqli
								(
								AppConfig::get("serveraddr"),
								AppConfig::get("username"),
								AppConfig::get("password"),
								AppConfig::get("dbname")
								);
		if (self::$conn->connect_error) {
			die("Connection failed: " . self::$conn->connect_error);
		}
        self::$conn->set_charset("utf8");

	}
    
    public function __destruct() {
		//self::$conn->close();

	}

	public static function getUserByUsername($user_username) {

		$stmt = self::$conn->prepare("SELECT 
        user_id, 
        user_username, 
        user_realname, 
        user_pass,
        user_email, 
        user_session_id, 
        user_privilege 
        FROM users WHERE user_username = ?;");

		$stmt->bind_param("s", $user_username);

		$stmt->execute();

		$stmt->bind_result(
								$b_user_id,
                                $b_user_username,
                                $b_user_realname,
                                $b_user_pass,
								$b_user_email,
								$b_user_session_id,
								$b_user_privilege
								);
		$stmt->fetch();
		$to_return = array(
			'user_id' 		=>	$b_user_id,
            'user_username' => $b_user_username,
            'user_realname' 	=>	$b_user_realname,
			'user_email'		=>	$b_user_email,
			'user_pass'		=>	$b_user_pass,
			'user_session_id' =>	$b_user_session_id,
			'user_privilege'	=>	$b_user_privilege
		);
        $stmt->close();
		return $to_return;
	}
    
    public static function getUserById($user_id) {

		$stmt = self::$conn->prepare("SELECT 
        user_id, 
        user_username, 
        user_realname, 
        user_pass,
        user_email, 
        user_session_id, 
        user_privilege 
        FROM users WHERE user_id = ?;");

		$stmt->bind_param("i", $user_id);

		$stmt->execute();

		$stmt->bind_result(
								$b_user_id,
                                $b_user_username,
                                $b_user_realname,
                                $b_user_pass,
								$b_user_email,
								$b_user_session_id,
								$b_user_privilege
								);
		$stmt->fetch();
		$to_return = array(
			'user_id' 		=>	$b_user_id,
            'user_username' => $b_user_username,
            'user_realname' 	=>	$b_user_realname,
			'user_email'		=>	$b_user_email,
			'user_pass'		=>	$b_user_pass,
			'user_session_id' =>	$b_user_session_id,
			'user_privilege'	=>	$b_user_privilege
		);
        $stmt->close();
		return $to_return;
	}
    
    public static function getUserBySessionId($user_session_id) {

        $stmt = self::$conn->prepare("SELECT user_id, user_username, user_realname, user_pass, user_email, user_session_id, user_privilege FROM users WHERE user_session_id = ?;");

        $stmt->bind_param("s", $user_session_id);

        $stmt->execute();

        $stmt->bind_result(
                                    $b_user_id,
                                    $b_user_username,
                                    $b_user_realname,
                                    $b_user_pass,
                                    $b_user_email,
                                    $b_user_session_id,
                                    $b_user_privilege
                                    );
        $stmt->fetch();
            $to_return = array(
                'user_id' 		=>	$b_user_id,
                'user_username' => $b_user_username,
                'user_realname' 	=>	$b_user_realname,
                'user_email'		=>	$b_user_email,
                'user_pass'		=>	$b_user_pass,
                'user_session_id' =>	$b_user_session_id,
                'user_privilege'	=>	$b_user_privilege
            );
        $stmt->close();
        return $to_return;
	}
    
    public static function setUserSessionId($user) {
 
        if (!($stmt = self::$conn->prepare("UPDATE users SET user_session_id = ? WHERE user_id = ?;"))){
            self::sendError($stmt->error);
        }
        
        $genSesId = $user->generateSessionId();
        $userId = $user->getUserId();
        
        if (!$stmt->bind_param("ss", $genSesId, $userId)){
            self::sendError($stmt->error);
        }
        
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
        return $genSesId;
    }
    
    public static function setUserStudent($userid,$realname,$username,$email,$class) {
        
        if (!($stmt = self::$conn->prepare("UPDATE users SET user_realname = ?, user_username = ? ,user_email = ? WHERE user_id = ?;
        
        "))) {
            self::sendError("Prepare failed");
        }
        
        if (!($stmt->bind_param("ssss", $realname, $username, $email, $userid))) {
            self::sendError("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);  
        }
        
        if (!$stmt->execute()) {
            self::sendError("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        }
        
        $stmt->close();
        
        if (!($stmt = self::$conn->prepare("UPDATE students SET student_class = ? WHERE student_user_id = ?;;
        
        "))) {
            self::sendError("Prepare failed");
        }
        
        if (!($stmt->bind_param("ss", $class, $userid))) {
            self::sendError("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);  
        }
        if (!$stmt->execute()) {
            self::sendError("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        }
        
        $stmt->close();
        
    }
    
    public static function setUserConsultant($userid,$realname,$username,$email) {
        $stmt = self::$conn->prepare("UPDATE users SET user_realname = ?, user_email = ?, user_username = ? WHERE user_id = ?;");
        $stmt->bind_param("ssss", $realname, $email, $username, $userid);
        $stmt->execute();
        $stmt->close();
        
    }
    
    public static function setUserPassword($userid,$password) {
        $stmt = self::$conn->prepare("UPDATE users SET user_pass = ? WHERE user_id = ?;");
        $hashed = password_hash($password, PASSWORD_DEFAULT);
        $stmt->bind_param("si",$hashed, $userid);
        $stmt->execute();
        $stmt->close();
    }
    
    public static function setUserRealName($userid,$realname){
        $stmt = self::$conn->prepare("UPDATE users SET user_realname = ? WHERE user_id = ?;");
        
        if (!$stmt->bind_param("si", $realname, $userid)){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    public static function setUserEmail($userid,$email){
        $stmt = self::$conn->prepare("UPDATE users SET user_email = ? WHERE user_id = ?;");
        if (!$stmt->bind_param("si", $email, $userid)){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    public static function resetPw($userid) {
        $stmt = self::$conn->prepare("UPDATE users SET user_pass = ? WHERE user_id = ?;");
        $hashed = password_hash("123", PASSWORD_DEFAULT);
        if (!$stmt->bind_param("si", $hashed, $userid)){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    public static function setStudentUploadSpec($userid,$file) {
        $stmt = self::$conn->prepare("UPDATE students SET student_upload_2 = ? WHERE student_user_id = ?;");
        if (!$stmt->bind_param("si", $file, $userid)){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    public static function setStudentSelectTopic($userid,$topicid,$file) {
        $stmt = self::$conn->prepare("UPDATE students SET student_topic_id = ?, student_upload = ?, student_accept = 0, student_upload_2 = NULL WHERE student_user_id = ?;");
        if (!$stmt->bind_param("isi", $topicid, $file, $userid)){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    public static function getAllByPrivilege($priv_niveau) {

        if ($priv_niveau == "student") {
            $result = self::$conn->query("SELECT users.user_id, users.user_username, users.user_email, users.user_realname, students.student_class FROM users, students WHERE user_privilege = 'student' AND students.student_user_id = users.user_id ORDER BY user_realname ASC;");
        }else {
            $result = self::$conn->query("SELECT user_id,  user_username, user_email, user_realname FROM users WHERE user_privilege = '".$priv_niveau."' ORDER BY user_realname ASC;");
        }
        
        while($row = $result->fetch_array())
        {
            $rows[] = $row;
        }
        
		return $rows;
    }
    
    public static function fetch_all_assoc(& $result,$index_keys) {
      $assoc = array();
      while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

            $pointer = & $assoc;

            for ($i=0; $i<count($index_keys); $i++) {
                    $key_name = $index_keys[$i];
                    if (!isset($row[$key_name])) {
                            print "Error: Key $key_name is not present in the results output.\n";
                            return(false);
                    }

                    $key_val= isset($row[$key_name]) ? $row[$key_name]  : "";
                    if (!isset($pointer[$key_val])) {              

                        $pointer[$key_val] = "";
                        $pointer = & $pointer[$key_val];
                    }
                    else {
                        $pointer = & $pointer[$key_val];
                    }

            }
            foreach ($row as $key => $val) {
                $pointer[$key] = $val;
            }

      }
      $result->close();
      return($assoc);              
    }
    
    public static function getClassList() {
        $sql = "SELECT student_class FROM students GROUP BY student_class";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                if ($row['student_class'] != "")
                $array[] = $row['student_class'];
            }
        } else {
            return null; //do nothing
        }
        
        return $array;
    }
    
    public static function getProtocols($userid) {
        $sql = "SELECT * FROM protocols WHERE protocol_student_user_id = ".$userid.";";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
        return $array;
    }
    
    public static function setProtocols(
        $userid,
        $finaltitle,
        $pos1,
        $mis1,
        $rec1,
        $progready1,
        $docsready1,
        $pos2,
        $mis2,
        $rec2,
        $progready2,
        $docsready2,
        $pos3,
        $mis3,
        $rec3,
        $progready3,
        $docsready3
        ) 
    {
        $stmt = self::$conn->prepare(
            "UPDATE protocols 
            SET 
            protocol_final_title = ?,
            protocol_1_pos = ?,
            protocol_1_mis = ?,
            protocol_1_rec = ?,
            protocol_1_prog_ready = ?,
            protocol_1_docu_ready = ?,
            protocol_2_pos = ?,
            protocol_2_mis = ?,
            protocol_2_rec = ?,
            protocol_2_prog_ready = ?,
            protocol_2_docu_ready = ?,
            protocol_3_pos = ?,
            protocol_3_mis = ?,
            protocol_3_rec = ?,
            protocol_3_prog_ready = ?,
            protocol_3_docu_ready = ?
            WHERE protocol_student_user_id = ?;");
        if (!$stmt->bind_param("ssssiisssiisssiii",
                               
        $finaltitle,
        $pos1,
        $mis1,
        $rec1,
        $progready1,
        $docsready1,
        $pos2,
        $mis2,
        $rec2,
        $progready2,
        $docsready2,
        $pos3,
        $mis3,
        $rec3,
        $progready3,
        $docsready3,
        $userid
                )
           ){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    public static function setExamReady($userid,$val) {
        
        if ($val == 0){
            $stmt = self::$conn->prepare("UPDATE protocols SET protocol_exam_ready = 1 WHERE protocol_student_user_id = ?;");
        }else{
            $stmt = self::$conn->prepare("UPDATE protocols SET protocol_exam_ready = 0 WHERE protocol_student_user_id = ?;");
        }
        
        if (!$stmt->bind_param("i", $userid)){
            self::sendError($stmt->error);
        }
        
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    public static function deleteUser($user_id) {
        
        $stmt = self::$conn->prepare("DELETE FROM protocols WHERE protocol_student_user_id = ?;");
        if (!($stmt->bind_param("i", $user_id))) {
            self::sendError("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);  
        }
        
        if (!$stmt->execute()) {
            self::sendError("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        }
        $stmt->close();
        
        $stmt = self::$conn->prepare("DELETE FROM students WHERE student_user_id = ?;");
        if (!($stmt->bind_param("i", $user_id))) {
            self::sendError("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);  
        }
        
        if (!$stmt->execute()) {
            self::sendError("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        }
        $stmt->close();
        
        $stmt = self::$conn->prepare("DELETE FROM topics WHERE topic_owner_user_id = ?;");
        if (!($stmt->bind_param("i", $user_id))) {
            self::sendError("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);  
        }
        
        if (!$stmt->execute()) {
            self::sendError("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        }
        $stmt->close();
        
        
        $stmt = self::$conn->prepare("DELETE FROM users WHERE user_id = ?;");
        if (!($stmt->bind_param("i", $user_id))) {
            self::sendError("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);  
        }
        
        if (!$stmt->execute()) {
            self::sendError("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        }
        $stmt->close();
        
        
        
        
    }
    
    public static function addUser($username, $password, $realname, $email, $priv, $class) {
        
        if (!($stmt = self::$conn->prepare("INSERT INTO users ( user_username, user_pass, user_realname, user_email, user_privilege) VALUES (?, ?, ?, ?, ?);"))) {
            self::sendError("addUserError1");
        }
        if (empty($password)) $password = "123";
        $hashed = password_hash($password, PASSWORD_DEFAULT);
        if (!$stmt->bind_param("sssss", $username, $hashed, $realname, $email, $priv)) {
            self::sendError("addUserError2");
        }
        if (!$stmt->execute()) {
            return "error";
        }
        $stmt->close();
        
        $stmt = self::$conn->prepare("SELECT user_id FROM users WHERE user_username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt->bind_result($userid);
        $stmt->fetch();
        $stmt->close();
        
        
        if ($priv == "student") {
            
            if (!($stmt = self::$conn->prepare("INSERT INTO students (student_user_id, student_class) VALUES (?, ?);"))) {
                self::sendError("addUserError1");
            }

            if (!$stmt->bind_param("is", $userid, $class)) {
                self::sendError("addUserError2");
            }
            if (!$stmt->execute()) {
                self::sendError($stmt->error);
            }


            $stmt->close();
            
            if (!($stmt = self::$conn->prepare("INSERT INTO `protocols` (`protocol_student_user_id`, `protocol_final_title`, `protocol_1_pos`, `protocol_1_mis`, `protocol_1_rec`, `protocol_1_prog_ready`, `protocol_1_docu_ready`, `protocol_2_pos`, `protocol_2_mis`, `protocol_2_rec`, `protocol_2_prog_ready`, `protocol_2_docu_ready`, `protocol_3_pos`, `protocol_3_mis`, `protocol_3_rec`, `protocol_3_prog_ready`, `protocol_3_docu_ready`) VALUES ( ?, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);"))) {
                self::sendError("addUserError1");
            }

            if (!$stmt->bind_param("i", $userid)) {
                self::sendError("addUserError2");
            }
            if (!$stmt->execute()) {
                self::sendError($stmt->error);
            }
            
                $stmt->close();

        }
        
        
    }
    
    public static function getMyTopics($userid) {
        $sql = "SELECT * FROM topics WHERE topic_owner_user_id = ".$userid.";";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
        
        return $array;
        
    }
    
    public static function getMyTopic($userid) {
        $sql = "SELECT * FROM topics, students, users WHERE student_user_id = ".$userid." AND student_topic_id = topic_id AND topic_owner_user_id = user_id;";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
        
        return $array;
    }
    
    public static function getApproved($userid) {
        $sql = "SELECT student_accept FROM students WHERE student_user_id = ".$userid.";";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            //do nothing
        }
        
        return $array;
    }
    
    public static function getUserSessionId($userid) {
        $sql = "SELECT user_session_id FROM users WHERE user_id = ".$userid.";";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
        
        return $array;
    }
    
    public static function setTopic($topicid, $topictitle, $topicrecommtech, $topicdesc) {
        if (!($stmt = self::$conn->prepare("UPDATE topics SET topic_title = ?, topic_recomm_tech = ?, topic_desc = ? WHERE topic_id = ?;"))) {
            self::sendError("Error2");
        }
        if (!$stmt->bind_param("sssi", $topictitle, $topicrecommtech, $topicdesc, $topicid)) {
            self::sendError("Error1");
        }
        if (!$stmt->execute()) {
            self::sendError($stmt->error);
        }
        $stmt->close();
    }
    
    public static function addTopic($topictitle, $topicrecommtech, $topicdesc, $topicowner) {
        if (!($stmt = self::$conn->prepare("INSERT INTO topics (topic_title, topic_recomm_tech, topic_desc, topic_owner_user_id) VALUES (?, ?, ?, ?);"))) {
            self::sendError("Error2");
        }
        if (!$stmt->bind_param("sssi", $topictitle, $topicrecommtech, $topicdesc, $topicowner)) {
            self::sendError("Error1");
        }
        if (!$stmt->execute()) {
            self::sendError($stmt->error." - ".$topicowner);
        }
        $stmt->close();
    }
    
    public static function delTopic($topicid) {
        $stmt = self::$conn->prepare("DELETE FROM topics WHERE topic_id = ?;");
        if (!($stmt->bind_param("i", $topicid))) {
            self::sendError("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);  
        }
        
        if (!$stmt->execute()) {
            self::sendError("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        }
        $stmt->close();
    }
    
    public static function getAllTopics() {
        $sql = "SELECT * FROM topics;";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
       
        return $array;
    }
    
    public static function getSummary() {
        $sql = "SELECT * FROM students, users WHERE students.student_user_id = users.user_id;";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
       
        return $array;
    }
    
    public static function getAcceptedStudentsForConsultant($userid) {
        $sql = "SELECT * FROM topics, users, students WHERE topic_owner_user_id = ".$userid."  AND student_topic_id = topic_id AND student_accept = 1 AND student_user_id = user_id;";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
        return $array;
    }
    
    public static function getPms($userid) {
        $sql = "SELECT * FROM pms WHERE pm_recipient_user_id = ".$userid." OR  pm_sender_user_id = ".$userid." ORDER BY pm_time DESC;";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
       
        return $array;
    }
    
    public static function isPmRead($pmid) {
        $sql = "SELECT pm_unread FROM pms WHERE pm_id = ".$pmid.";";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
        
        return $array;
    }
    
    public static function setPmsRead($userid) {
        if (!($stmt = self::$conn->prepare("UPDATE pms SET pm_unread = 0 WHERE pm_recipient_user_id = ?;"))) {
            self::sendError("Error2");
        }
        if (!$stmt->bind_param("i", $userid)) {
            self::sendError("Error1");
        }
        if (!$stmt->execute()) {
            self::sendError($stmt->error);
        }
        $stmt->close();
    }
    
       public static function setPmsReadFrom($recipient,$sender) {
        if (!($stmt = self::$conn->prepare("UPDATE pms SET pm_unread = 0 WHERE pm_recipient_user_id = ?;"))) {
            self::sendError("Error2");
        }
        if (!$stmt->bind_param("i", $userid)) {
            self::sendError("Error1");
        }
        if (!$stmt->execute()) {
            self::sendError($stmt->error);
        }
        $stmt->close();
    }
    
    public static function getUnread($userid) {
        $sql = "SELECT COUNT(pm_unread) AS unread FROM pms WHERE pm_recipient_user_id = ".$userid." AND pm_unread = 1;";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
        return $array;
    }
    
    public static function getUnreadFrom($recipient,$sender) {
        $sql = "SELECT COUNT(pm_unread) AS unread FROM pms WHERE pm_sender_user_id = ".$sender." AND pm_recipient_user_id = ".$recipient." AND pm_unread = 1;";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
        return $array;
    }
    
    
    public static function sendPms($recip,$sender,$msg) {
        if (!($stmt = self::$conn->prepare("INSERT INTO pms (pm_recipient_user_id, pm_sender_user_id, pm_message) VALUES (?, ?, ?);"))) {
            self::sendError("Error2");
        }
        if (!$stmt->bind_param("iis", $recip, $sender, $msg)){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    public static function getAppliedStudentsForTopic($topicid) {
        $sql = "SELECT * FROM students, topics, users WHERE student_topic_id = ".$topicid." AND user_id = student_user_id AND topic_id = student_topic_id AND student_accept = 0;";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
       
        return $array;
    } 
    
    public static function getDeadlineList() {
        $sql = "SELECT * FROM deadlines ORDER BY deadline_due ASC;";
        $result = self::$conn->query($sql);
        $array = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $array[] = $row;
            }
        } else {
            return null; //do nothing
        }
       
        return $array;
    }
    
    public static function addDeadLine($dltitle,$dldue,$dldesc) {
        if (!($stmt = self::$conn->prepare("INSERT INTO deadlines (deadline_title, deadline_due, deadline_desc) VALUES (?, ?, ?);"))) {
            self::sendError("Error2");
        }
        if (!$stmt->bind_param("sss", $dltitle, $dldue, $dldesc)) {
            self::sendError("Error1");
        }
        if (!$stmt->execute()) {
            self::sendError($stmt->error." - ".$topicowner);
        }
        $stmt->close();
    }
    
        public static function delDeadLine($dlid) {
        $stmt = self::$conn->prepare("DELETE FROM deadlines WHERE deadline_id = ?;");
        if (!($stmt->bind_param("i", $dlid))) {
            self::sendError("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);  
        }
        
        if (!$stmt->execute()) {
            self::sendError("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
        }
        $stmt->close();
    }
    
    public static function acceptStudentTopic($topicid, $studentid) {
        $stmt = self::$conn->prepare("UPDATE students SET student_accept = 1 WHERE student_topic_id = ? AND student_user_id = ?;");
        if (!$stmt->bind_param("ii", $topicid, $studentid)){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }

    }
    
    public static function declineStudentTopic($topicid,$studentid) {
        $stmt = self::$conn->prepare("UPDATE students SET student_accept = -1 WHERE student_topic_id = ? AND student_user_id = ?;");
        if (!$stmt->bind_param("ii", $topicid, $studentid)){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    public static function sendError($error) {
        
        ?>

    <form id="errorForm" action="/pages/error" method="post">
        <input type="hidden" name="errorMsg" value="<?php echo $error; ?>">
    </form>
    <script type="text/javascript">
        document.getElementById('errorForm').submit();
    </script>
    <?php
    }
    
    public static function generateRandomString($length = 32) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $toReturn = '';
        for ($i = 0; $i < $length; $i++) {
            $toReturn .= $characters[rand(0, $charactersLength - 1)];
        }
        return $toReturn;
    }
    
    public static function getUpload2($userid){
        return self::$conn->query("SELECT student_upload_2 FROM students WHERE student_user_id = ".$userid.";")->fetch_object()->student_upload_2;  
    }
    
    public static function getCloud($userid){
        return self::$conn->query("SELECT student_cloud FROM students WHERE student_user_id = ".$userid.";")->fetch_object()->student_cloud;  
    }
    
    public static function setCloud($userid,$cloudlink){
        $stmt = self::$conn->prepare("UPDATE students SET student_cloud = ? WHERE student_user_id = ?;");
        if (!$stmt->bind_param("si", $cloudlink, $userid)){
            self::sendError($stmt->error);
        }
        if (!$stmt->execute()){
            self::sendError($stmt->error);
        }
        if (!$stmt->close()){
            self::sendError($stmt->error);
        }
    }
    
    
}
?>