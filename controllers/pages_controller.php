<?php

class PagesController {
    
    public $lang;
    public $current_user;
    
    public function __construct() {
        if (isset($_SESSION['current_lang'])) {
                $this->lang = new Lang($_SESSION['current_lang']);
        }
        
        if ( isset($_SESSION['current_user']) ){
            
            
            
            $this->current_user = unserialize($_SESSION['current_user']);
            $sessid = Database::getUserSessionId($this->current_user->getUserId());
            
            if ($this->current_user->getSessionId() != $sessid[0]['user_session_id']){
                unset($_SESSION['current_user']);
                self::sendError("errorMultDev");
            }
            
            
        }
        
    }
    
	public function home() {
        if (self::isSetCurrentUser())
		switch ($this->current_user->getPrivilege()){
            case "student":
                $this->mytopic();
                break;
            case "consultant":
                $this->mytopics();
                break;
            case "admin":
                $this->announcedtopics();
                break;
        };
        
	}

	public function login() {
        if (AppConfig::get("setup")=="false") {
            header("Location: /install.php");
            die();
        } else {
		  include_once('views/pages/login.php');
        }
	}
    
    
    //common site
    public function announcedtopics() {
        if (self::isSetCurrentUser()) 
        if ($this->current_user->getPrivilege() == "consultant"){
            require_once('views/menu/consultant.php');
            require_once('classes/Topics.class.php');
            
            require_once('models/announcedtopics.php');
            require_once('views/pages/announcedtopics.php');
        }
        elseif ($this->current_user->getPrivilege() == "student") {
            require_once('views/menu/student.php');
            require_once('classes/Topics.class.php');
            
            require_once('models/announcedtopics.php');
            require_once('views/pages/announcedtopics.php');
        }
        elseif ($this->current_user->getPrivilege() == "admin") {
            require_once('views/menu/admin.php');
            require_once('classes/Topics.class.php');
            
            require_once('models/announcedtopics.php');
            require_once('views/pages/announcedtopics.php');
        }
        else{$this->error();}
    }
    public function summary() {
        if (self::isSetCurrentUser())
        if ($this->current_user->getPrivilege() == "consultant"){
            require_once('views/menu/consultant.php');
            require_once('models/summary.php');
            require_once('views/pages/summary.php');
        }
        elseif ($this->current_user->getPrivilege() == "admin") {
            require_once('views/menu/admin.php');
            require_once('models/summary.php');
            require_once('views/pages/summary.php');
        }
        else{$this->error();};
    }
    public function messaging() {
        if (self::isSetCurrentUser())
        if ($this->current_user->getPrivilege() == "consultant"){
            
            require_once('models/messaging.php');
            require_once('views/menu/consultant.php');
            require_once('views/pages/messaging_consultant.php');
        }
        elseif ($this->current_user->getPrivilege() == "student") {
            
            require_once('models/messaging.php');
            require_once('views/menu/student.php');
            require_once('views/pages/messaging_student.php');
        }
        else{$this->error();};
    }
    public function protocol() {
        if (self::isSetCurrentUser())
        if ($this->current_user->getPrivilege() == "consultant"){
            
            require_once('models/protocol.php');
            require_once('views/menu/consultant.php');
            require_once('views/pages/protocol.php');
        };
    }
    public function manage(){
        if (self::isSetCurrentUser()) {
            require_once("models/manage.php");
            if ($this->current_user->getPrivilege() == "consultant"){
                require_once('views/menu/consultant.php');
            }
            elseif ($this->current_user->getPrivilege() == "student") {
                require_once('views/menu/student.php');
            }
            elseif ($this->current_user->getPrivilege() == "admin") {
                require_once('views/menu/admin.php');
            }
            else{$this->error();
            }
            require_once('views/pages/manage.php');
        }
    }
    
    
    //below student sites
    public function mytopic() {
        if (self::isSetCurrentUser())
        if ($this->current_user->getPrivilege() == "student") {
            require_once('models/mytopic.php');
            require_once('views/menu/student.php');
            require_once('views/pages/mytopic.php');
        }else{$this->error();};
        
    }
    //above student sites
    
    //below consultant sites
    public function mytopics() {
        if (self::isSetCurrentUser())
        if ($this->current_user->getPrivilege() == "consultant") {
            
            require_once('views/menu/consultant.php');
            require_once('classes/Topics.class.php');
            
            require_once('models/mytopics.php');
            require_once('views/pages/mytopics.php');
        }else{$this->error();};
    }
    //above consultant sites
    
    //below admin sites
    public function topiclist() {
        if (self::isSetCurrentUser())
        if ($this->current_user->getPrivilege() == "admin") {
            require_once('views/menu/admin.php');   
        }else{$this->error();};
    }
    
    public function studentlist() {
        if (self::isSetCurrentUser())
        if ($this->current_user->getPrivilege() == "admin") {
            require_once('views/menu/admin.php');   
            require_once('classes/Students.class.php');
            
            
            require_once('models/studentlist.php');
            require_once('views/pages/studentlist.php');

        }else{$this->sendError("privilegeError");};
    }
    
    public function consultantlist() {
        if (self::isSetCurrentUser())
        if ($this->current_user->getPrivilege() == "admin") {
            
            require_once('views/menu/admin.php');
            require_once('classes/Consultants.class.php');
            
            require_once('models/consultantlist.php');
            require_once('views/pages/consultantlist.php');
            
        }else{$this->error();};
    }
    
    public function deadlinelist() {
        if (self::isSetCurrentUser())
        if ($this->current_user->getPrivilege() == "admin") {
            require_once('views/menu/admin.php');  
            require_once('models/deadlinelist.php');
            require_once('views/pages/deadlinelist.php');
        }else{$this->error();};
    }
    //above admin sites
    
    public function error() {
        require_once('views/pages/error.php');
    }
    
    public function sendError($error) {
        ?>
    <form id="errorForm" action="/pages/error" method="post">
        <input type="hidden" name="errorMsg" value="<?php echo self::getText($error); ?>">
        <input type="hidden" name="goBack" value="<?php echo self::getText("goBack"); ?>">
    </form>
    <script type="text/javascript">
        document.getElementById('errorForm').submit();
    </script>
    <?php
    }
    
    private function getText($text){
        return $this->lang->get($text);
    }
    
    private function isSetCurrentUser() {
        if (isset($this->current_user)){
            return true;
        }else{
            self::sendError("notLoggedId");
            return false;
        }
    }

}

?>