<img src="/res/loading.gif" style="margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);">
<?php



class AuthController {

	public function login() {
		$user_array = Database::getUserByUsername($_POST['l_id']);
		$user = new User($user_array);
        Database::setUserSessionId($user);
            if (password_verify($_POST['l_pwd'],$user->getPass())) {
                $_SESSION['current_user'] = serialize($user);
                $_SESSION['current_lang'] = $_POST['l_lang'];
                header('Location: /pages/home');
            }else{
                unset($user);
                $this->error("loginErr");
            }
	}

	public function logout(){
		unset($_SESSION['current_user']);
		header('Location: /');
	}

	public function error($error) {
        ?>
    <form id="errorForm" action="/pages/error" method="post">
        <input type="hidden" name="errorMsg" value="<?php echo $error; ?>">
    </form>
    <script type="text/javascript">
        document.getElementById('errorForm').submit();
    </script>
    <?php
	}
}

?>