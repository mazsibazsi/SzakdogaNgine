<script>
    document.title = "<?php echo $this->getText("studentList"); ?>" + name;
</script>
<?php

if (isset($_POST['intent'])){
    if ($_POST['intent'] == "update") {
        Database::setUserStudent($_POST['UserId'],$_POST['modRealName'],$_POST['modUserName'],$_POST['modEmail'],$_POST['modClass']);
        if (isset($_POST['resetPw']))
        if ($_POST['resetPw'] == "do") {
            Database::resetPw($_POST['UserId']);
        } ;
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulEditing')."</strong></div>";
    }
    if ($_POST['intent'] == "delete") {
        Database::deleteUser($_POST['UserId']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulDeletion')."</strong></div>";
    }
    if ($_POST['intent'] == "insert") {
        Database::addUser( $_POST['newUserName'], $_POST['newUserPassword'], $_POST['newRealName'], $_POST['newEmail'],  "student", $_POST['newClass']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulAddition')."</strong></div>";
    }
    if ($_POST['intent'] == "csvUpload") {
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["csvUploadFile"]["name"]);
        $uploadOk = 1;
        $docFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $target_file = $target_dir . Database::generateRandomString() . "." . $docFileType;
        move_uploaded_file($_FILES["csvUploadFile"]["tmp_name"], $target_file);
            
        $row = 1;
        $errors = 0;
        $rows = 0;
        if (($handle = fopen($target_file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                
                $realname=$data[0];
                $username=$data[1];
                $class=$data[2];
                $password="";
                $email="";
                if (isset($data[3])) $password=$data[3];
                if (isset($data[4])) $email=$data[4];
                if (Database::addUser($username,$password,$realname,$email,"student",$class) == "error"){
                    $errors++;
                }
                $rows++;
                $row++;
            }
            
            if (sizeof($errors) != 0 and $errors != $rows)
                $message = "<div class='alert alert-warning'><strong>".$this->getText('importSuccessError')." ".$errors."</strong></div>";
            if ($errors == $rows)
                $message = "<div class='alert alert-danger'><strong>".$this->getText('importFailed')."</strong></div>";
            if ($errors == 0)
                $message = "<div class='alert alert-success'><strong>".$this->getText('importSuccess')."</strong></div>";
            fclose($handle);
        }
        
    }
}

$students = new Students();

$student_list = $students->getList();

?>