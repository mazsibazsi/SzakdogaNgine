<script>
    document.title = "<?php echo $this->getText("announcedTopics"); ?>" + name;
</script>
<?php


if (isset($_POST['intent'])){
    //TODO Saját téma hozzáadása
    if ($_POST['intent'] == "selectTopic") {
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $docFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $target_file = $target_dir . Database::generateRandomString() . "." . $docFileType;
        if (file_exists($target_file)) {
            $uploadOk = 0;
            $message = $this->getText('fileAlreadyExists');
            $message = "<div class='alert alert-warning'><strong>".$this->getText('fileAlreadyExists')."</strong></div>";
        }else{
            if($docFileType != "doc" && $docFileType != "docx" && $docFileType != "odt"
                && $docFileType != "rtf" && $docFileType != "pdf" ) {
                    $message = $this->getText('wrongFileType');
                    $message = "<div class='alert alert-danger'><strong>".$this->getText('wrongFileType')."</strong></div>";
                    $uploadOk = 0;
            }else{
                if ($uploadOk == 0) {
                    $message = "<div class='alert alert-warning'><strong>".$this->getText('noSuccesFileUpload')."</strong></div>";
                // if everything is ok, try to upload file
                } else {
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                        $message = "<div class='alert alert-success'><strong>".$this->getText('succesfulFileUpload')."</strong></div>";
                        Database::setStudentSelectTopic($this->current_user->getUserId(),$_POST['topicId'],$target_file);
                        header("Location: /pages/home");
                        //Database::setTopicStudent();
                    } else {
                       $message = "<div class='alert alert-warning'><strong>".$this->getText('noSuccesFileUpload')."</strong></div>";
                    }
                }
            }

        }
        
    }
    
}

$announcedtopics = new Topics();

$announcedtopics->getAllTopics();

$announcedtopics_list = $announcedtopics->getList();




?>