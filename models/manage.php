<script>
    document.title = "<?php echo $this->getText("myId"); ?>" + name;
</script>
<?php

if (isset($_POST['intent'])){
    if ($_POST['intent'] == "changeEmail") {
        Database::setUserEmail($this->current_user->getUserId(), $_POST["chEmail"]);
        header('Location: /auth/logout');
    }
    if ($_POST['intent'] == "changePw") {
        Database::setUserPassword($this->current_user->getUserId(), $_POST["chPw"]);
        header('Location: /auth/logout');
    }
    if ($_POST['intent'] == "changeCloud") {
        Database::setCloud($this->current_user->getUserId(), $_POST["chCl"]);
    }
}

?>