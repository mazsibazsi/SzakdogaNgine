<script>
    document.title = "<?php echo $this->getText("consultantList"); ?>" + name;
</script>
<?php

if (isset($_POST['intent'])){
    if ($_POST['intent'] == "update") {
        Database::setUserConsultant($_POST['UserId'],$_POST['modRealName'],$_POST['modUserName'],$_POST['modEmail']);
        if (isset($_POST['resetPw']))
        if ($_POST['resetPw'] == "do") {
            Database::resetPw($_POST['UserId']);
        } ;
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulEditing')."</strong></div>";
    }
    if ($_POST['intent'] == "delete") {
        Database::deleteUser($_POST['UserId']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulDeletion')."</strong></div>";
    }
    if ($_POST['intent'] == "insert") {
        Database::addUser( $_POST['newUserName'], $_POST['newUserPassword'], $_POST['newRealName'], $_POST['newEmail'], "consultant", "");
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulAddition')."</strong></div>";
    }
}

$consultants = new Consultants();

$consultant_list = $consultants->getList();

?>