<script>
    document.title = "<?php echo $this->getText("myTopic"); ?>" + name;
</script>
<?php
if (isset($_POST['intent'])){
    if ($_POST['intent'] == "upload2") {
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $docFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $target_file = $target_dir . Database::generateRandomString() . "." . $docFileType;
        if (file_exists($target_file)) {
            $uploadOk = 0;
            $message = $this->getText('fileAlreadyExists');
            $message = "<div class='alert alert-warning'><strong>".$this->getText('fileAlreadyExists')."</strong></div>";
        }else{
            if($docFileType != "doc" && $docFileType != "docx" && $docFileType != "odt"
                && $docFileType != "rtf" && $docFileType != "pdf" ) {
                    $message = $this->getText('wrongFileType');
                    $message = "<div class='alert alert-danger'><strong>".$this->getText('wrongFileType')."</strong></div>";
                    $uploadOk = 0;
            }else{
                if ($uploadOk == 0) {
                    $message = "<div class='alert alert-warning'><strong>".$this->getText('noSuccesFileUpload')."</strong></div>";
                // if everything is ok, try to upload file
                } else {
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                        $message = "<div class='alert alert-success'><strong>".$this->getText('succesfulFileUpload')."</strong></div>";
                        Database::setStudentUploadSpec($this->current_user->getUserId(),$target_file);
                    } else {
                       $message = "<div class='alert alert-warning'><strong>".$this->getText('noSuccesFileUpload')."</strong></div>";
                    }
                }
            }

        }
    }
}

$mytopic = Database::getMyTopic($this->current_user->getUserId());
$deadlinelist = Database::getDeadlineList();

?>