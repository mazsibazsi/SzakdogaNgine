<script>
    document.title = "<?php echo $this->getText("myTopics"); ?>" + name;
</script>
<?php

if (isset($_POST['intent'])){
    //TODO Saját téma hozzáadása
    if ($_POST['intent'] == "update") {
        Database::setTopic($_POST['topicId'],$_POST['topicTitle'],$_POST['topicRecommTech'],$_POST['topicDescription']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulEditing')."</strong></div>";
    }
    if ($_POST['intent'] == "delete") {
        Database::delTopic($_POST['topicId']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulDeletion')."</strong></div>";
    }
    if ($_POST['intent'] == "insert") {
        Database::addTopic($_POST['topicTitle'], $_POST['topicRecommTech'],$_POST['topicDescription'], $_POST['topicOwner']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('newTopicAdded')."</strong></div>";
    }
    if ($_POST['intent'] == "accept") {
        Database::acceptStudentTopic($_POST['topicId'],$_POST['studentId']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulOperation')."</strong></div>";
    }
    if ($_POST['intent'] == "decline") {
        Database::declineStudentTopic($_POST['topicId'],$_POST['studentId']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulOperation')."</strong></div>";
    }
    
}

$mytopics = new Topics();

$mytopics->getMyTopics($this->current_user->getUserId());

$mytopics_list = $mytopics->getList();

?>