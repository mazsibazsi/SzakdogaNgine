<script>
    document.title = "<?php echo $this->getText("protocol"); ?>" + name;
</script>
<?php

if (isset($_POST['intent']) and $_POST['intent'] == "update"){
    Database::setProtocols(
        $_POST['selectedStudent'],
        $_POST['finalTitle'],
        $_POST['1_pos'],
        $_POST['1_mis'],
        $_POST['1_rec'],
        $_POST['1_prog_comp'],
        $_POST['1_docs_comp'],
        $_POST['2_pos'],
        $_POST['2_mis'],
        $_POST['2_rec'],
        $_POST['2_prog_comp'],
        $_POST['2_docs_comp'],
        $_POST['3_pos'],
        $_POST['3_mis'],
        $_POST['3_rec'],
        $_POST['3_prog_comp'],
        $_POST['3_docs_comp']
    );
}

if (isset($_POST['val']) ){
    Database::setExamReady($_POST['selectedStudent'],$_POST['val']);
}


$myacceptedstudents = Database::getAcceptedStudentsForConsultant($this->current_user->getUserId());

if (isset($_POST['selectedStudent']) and $_POST['selectedStudent'] != "sel") $selectedStudent = Database::getProtocols($_POST['selectedStudent'])[0];
    


?>