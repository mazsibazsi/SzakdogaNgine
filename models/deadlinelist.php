<script>
    document.title = "<?php echo $this->getText("deadlineList"); ?>" + name;
</script>
<?php

if (isset($_POST['intent'])) {
    if ($_POST['intent'] == "insert") {
        Database::addDeadLine($_POST['dlTitle'],$_POST['dlDue'],$_POST['dlDesc']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulAddition')."</strong></div>";
    }
    if ($_POST['intent'] == "delete") {
        Database::delDeadLine($_POST['dlId']);
        $message = "<div class='alert alert-success'><strong>".$this->getText('successfulDeletion')."</strong></div>";
    }
}


$deadlinelist = Database::getDeadlineList();

?>