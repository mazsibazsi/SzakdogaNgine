<script>
    document.title = "<?php echo $this->getText("messaging"); ?>" + name;
</script>
<?php

if (isset($_POST['intent'])){
    if ($_POST['intent'] == "send") {
        Database::sendPms($_POST['msgWith'],$this->current_user->getUserId(),$_POST['message']);
    }
}

if ($this->current_user->getPrivilege() == "consultant") {
    $myacceptedstudents = Database::getAcceptedStudentsForConsultant($this->current_user->getUserId());
    $pms = Database::getPms($this->current_user->getUserId());
    if (isset($_POST['msgWith'])){
        Database::setPmsRead($this->current_user->getUserId(),$_POST['msgWith']);
    }
    
}

if ($this->current_user->getPrivilege() == "student") {
    $myconsultant = Database::getMyTopic($this->current_user->getUserId())[0];
    $myconsultantid = $myconsultant['user_id'];
    $pms = Database::getPms($this->current_user->getUserId());
    Database::setPmsRead($this->current_user->getUserId());
    
}



?>