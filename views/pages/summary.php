<div id="content" class="container container-table">
    <h3 style="text-align: center;">
        <?php if (isset($message)) echo $message; ?>
    </h3>
    <input type="text" class="form-control" id="searchText" onkeyup="search()" placeholder="<?php echo $this->getText('search'); ?>">
    <table id="table" class="table table-bordered table-striped table-condensed" style="#width:100%">
        <tr>
            <style>
                th {
                    width: auto;
                    text-align: center;
                    padding-top: 16px;
                }

                td {
                    text-align: center;
                }
            </style>
            <th>
                <?php echo $this->getText('studentName'); ?>
            </th>
            <th>
                <?php echo $this->getText('userClass'); ?>
            </th>
            <th>
                <?php echo $this->getText('topicTitle'); ?>
            </th>
            <th>
                <?php echo $this->getText('cloudLink'); ?>
            </th>
            <th>
                <?php echo $this->getText('examReady'); ?>
            </th>
        </tr>
        <?php
        for ($i = 0; $i < sizeof($summary); $i++) {
            ?>
            <tr>

                <td>
                    <a href="mailto:<?php echo $summary[$i]['user_email']; ?>">
                        <?php echo $summary[$i]['user_realname']; ?>
                    </a>
                </td>
                <td>
                    <?php echo $summary[$i]['student_class']; ?>
                </td>
                <td>
                    <?php 
                        if ($summary[$i]['student_topic_id'] != "") {
                            $mytopic = Database::getMyTopic($summary[$i]['user_id'])[0];
                            echo $mytopic['topic_title']." - ".$mytopic['user_realname'];
                        } else {
                            ?><span style="color:red;font-weight:bold;"><?php
                            echo $this->getText("noTopicSelectedYet");     
                    ?></span>
                    <?php
                        }
            
                    
                    ?>
                </td>
                <td>
                    <?php 
            if ($summary[$i]['student_cloud'] != ""){
                ?><a href="<?php echo $summary[$i]['student_cloud']; ?>"><span style="display:block;text-overflow: ellipsis;width: 300px;overflow: hidden; white-space: nowrap;"> <?php
                echo $summary[$i]['student_cloud']; ?></span></a>
                    <?php
            }else{
                ?><span style="color:red;font-weight:bold;"><?php
                echo $this->getText('cloudNotSetYet');
                ?></span>
                        <?php
            }
                    
                    ?>
                </td>
                <td>
                    <?php
                        if (Database::getProtocols($summary[$i]['student_user_id'])[0]["protocol_exam_ready"] == "0"){
                            echo $this->getText("no");
                        }else{
                            echo $this->getText("yes");
                        }
                    ?>
                               </td>
            </tr>
            <?php
            }
        ?>
    </table>
</div>

<script>
    function search() {
        // Declare variables
        var input, filter, table, tr, td0, td1, td2, td3, i;
        input = document.getElementById("searchText");
        filter = input.value.toUpperCase();
        table = document.getElementById("table");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td0 = tr[i].getElementsByTagName("td")[0];
            td1 = tr[i].getElementsByTagName("td")[1];
            td2 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            if (td0 || td1 || td2 || td3) {

                if (td0.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1) {

                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }

    }
</script>