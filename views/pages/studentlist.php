<div id="content" class="container container-table">
    <h3 style="text-align: center;">
        <?php if (isset($message)) echo $message; ?>
    </h3>
    <input type="text" class="form-control" id="searchText" onkeyup="search()" placeholder="<?php echo $this->getText('search'); ?>">
    <table id="table" class="table table-bordered table-striped table-condensed" style="#width:100%">
        <tr>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('userName'); ?>
            </th>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('realName'); ?>
            </th>

            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('emailAddr'); ?>
            </th>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('userClass'); ?>
            </th>
            <th style="">
                <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#newStudentModal"><?php echo $this->getText('newStudent'); ?></button>
            </th>
            <th>
                <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#csvImport">CSV</button>
            </th>
        </tr>
        <?php
        for ($i = 0; $i < sizeof($student_list); $i++) {
            ?>
            <tr>

                <td>
                    <?php echo $student_list[$i]['user_username']; ?>
                </td>

                <td>
                    <?php echo $student_list[$i]['user_realname']; ?>
                </td>

                <td>
                    <a href="mailto:<?php echo $student_list[$i]['user_email']; ?>">
                        <?php echo $student_list[$i]['user_email']; ?>
                    </a>
                </td>
                <td>
                    <?php echo $student_list[$i]['student_class']; ?>
                </td>
                <td colspan="2"><button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#editStudentModal<?php echo $student_list[$i]['user_id']; ?>"><?php echo $this->getText('edit'); ?></button></td>
            </tr>
            <?php
            }
        ?>
    </table>

    <?php
        for ($i = 0; $i < sizeof($student_list); $i++) {
            ?>
        <div id="editStudentModal<?php echo $student_list[$i]['user_id']; ?>" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <?php echo $this->getText('editingStudent'); ?>
                        </h5>
                        <br>&nbsp;
                        <form method="post">
                            <style>
                                .btn-link {
                                    border: none;
                                    outline: none;
                                    background: none;
                                    cursor: pointer;
                                    color: red;
                                    padding: 0;
                                    padding-top: 5px;
                                    text-decoration: none;
                                    font-family: inherit;
                                    font-size: inherit;
                                    margin: 0 auto;
                                    text-align: center;
                                }
                            </style>
                            <input type="hidden" name="UserId" value="<?php echo $student_list[$i]['user_id']; ?>">
                            <div>
                                <button type="submit" name="intent" value="delete" class="btn-link"><h6>&#128465;</h6></button>
                            </div>
                        </form>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post">
                        <div class="modal-body">
                            <h5>
                                <?php echo $this->getText('realName'); ?>
                            </h5> <input type="text" class="form-control" required name="modRealName" value="<?php echo $student_list[$i]['user_realname']; ?>"><br>
                            <h5>
                                <?php echo $this->getText('userName'); ?>
                            </h5> <input type="text" required readonly class="form-control" name="modUserName" required value="<?php echo $student_list[$i]['user_username']; ?>"><br>
                            <h5>
                                <?php echo $this->getText('emailAddr'); ?>
                            </h5> <input type="email" class="form-control" name="modEmail" value="<?php echo $student_list[$i]['user_email']; ?>"><br>
                            <h5>
                                <?php echo $this->getText('userClass'); ?>
                            </h5>

                            <input type="text required" autocomplete="off" class="form-control" list="classes" name="modClass" value="<?php echo $student_list[$i]['student_class']; ?>">
                            <datalist id="classes">
                                <input type="hidden" name="UserId" value="<?php echo $student_list[$i]['user_id']; ?>">
                                <?php
            
                                $classes = Database::getClassList();
                                for ($j = 0; $j < sizeof($classes); $j++) {
                                    echo "<option value='".$classes[$j]."'>";
                                }
                                ?>
                          </datalist>
                            <br>

                            <div class="form-group">
                                <div class="checkbox checbox-switch switch-primary">
                                    <label>
                                        <input type="checkbox" name="resetPw" value="do" />
                                        <span></span>
                                        <?php echo $this->getText("resetPw"); ?>
                                    </label>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">

                            <button type="submit" name="intent" value="update" class="btn btn-primary"><?php echo $this->getText('saveChanges'); ?></button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <?php
            
            }
        ?>



</div>

<!-- CSV MODAL START -->

<div id="csvImport" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">CSV Import</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
            </div>
            <form method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <style>
                        #csvtable {
                            border: 1px solid black;
                            border-collapse: collapse;
                            text-align: center;
                            margin-left: auto;
                            margin-right: auto;
                        }
                    </style>

                    <table id="csvtable">
                        <tr>
                            <th id="csvtable">A</th>
                            <th id="csvtable">B</th>
                            <th id="csvtable">C</th>
                            <th id="csvtable">(D)</th>
                            <th id="csvtable">(E)</th>
                        </tr>
                        <tr>
                            <td id="csvtable"><?php echo $this->getText("realName"); ?></td>
                            <td id="csvtable"><?php echo $this->getText("userName"); ?></td>
                            <td id="csvtable"><?php echo $this->getText("userClass"); ?></td>
                            <td id="csvtable">(<?php echo $this->getText("password"); ?>)</td>
                            <td id="csvtable">(EMail)</td>
                        </tr>
                    </table>
                    <br>
                    <h5 style="text-align:center;color:red;"><?php echo $this->getText("optionalFields"); ?></h5>
                    <br>
                    <input style="margin: auto;display: block;" type="file" name="csvUploadFile" id="csvUploadFile">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Import</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                </div>
                <input type="hidden" name="intent" value="csvUpload">
            </form>
        </div>
    </div>
</div>
<!-- CSV MODAL END -->

<!-- NEW STUDENT MODAL START -->
<div id="newStudentModal" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <?php echo $this->getText('newStudent'); ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <h5>
                        <?php echo $this->getText('realName'); ?>
                    </h5> <input type="text" required class="form-control" name="newRealName"><br>
                    <h5>
                        <?php echo $this->getText('userName'); ?>
                    </h5> <input type="text" required class="form-control" name="newUserName"><br>
                    <h5>
                        <?php echo $this->getText('password'); ?>
                    </h5> <input type="text" class="form-control" name="newUserPassword" placeholder="123"><br>
                    <h5>
                        <?php echo $this->getText('emailAddr'); ?>
                    </h5> <input type="email" class="form-control" name="newEmail"><br>
                    <h5>
                        <?php echo $this->getText('userClass'); ?>
                    </h5>
                    <input type="text" autocomplete="off" required class="form-control" list="classes" name="newClass" value="">
                    <datalist id="classes">
                                
                                <?php
            
                                $classes = Database::getClassList();
                                for ($j = 0; $j < sizeof($classes); $j++) {
                                    echo "<option value='".$classes[$j]."'>";
                                }
                                ?>
                          </datalist>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo $this->getText('newStudent'); ?></button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                </div>
                <input type="hidden" name="intent" value="insert">
            </form>
        </div>
    </div>
</div>
<!-- NEW STUDENT MODAL END -->



<script>
    function search() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("searchText");
        filter = input.value.toUpperCase();
        table = document.getElementById("table");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td0 = tr[i].getElementsByTagName("td")[0];
            td1 = tr[i].getElementsByTagName("td")[1];
            td2 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            if (td0 || td1 || td2 || td3) {
                if (td0.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>