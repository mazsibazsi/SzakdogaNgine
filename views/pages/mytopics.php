<div id="content" class="container container-table">
    <h3 style="text-align: center;">
        <?php if (isset($message)) echo $message; ?>
    </h3>
    <input type="text" class="form-control" id="searchText" onkeyup="search()" placeholder="<?php echo $this->getText('search'); ?>">
    <table id="table" class="table table-bordered table-striped table-condensed" style="#width:100%">
        <tr>
            <th style="width:60%;text-align:center;padding-top:16px;">
                <?php echo $this->getText('topicTitle'); ?>
            </th>
            <th style="" colspan="2">
                <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#newTopicModal"><?php echo $this->getText('newTopic'); ?></button>
            </th>
        </tr>
        <?php
        for ($i = 0; $i < sizeof($mytopics_list); $i++) {
            $appliedstudents = $mytopics->getAppliedStudents($mytopics_list[$i]['topic_id']);
            ?>
            <tr>

                <td style="text-align: center;">
                    <?php echo $mytopics_list[$i]['topic_title']; ?>
                </td>
                <td>
                    <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#checkTopicModal<?php echo $mytopics_list[$i]['topic_id']; ?>">
                    <?php
                    if (sizeof($appliedstudents) != 0){
                        echo "<span style='font-weight:bold;'>";
                    }
                    else{
                        echo "<span style='color: lightgray;'>";
                    }
                    echo $this->getText('checkTopicApplicants');
                    echo "</span>";
                    ?>
                    
                    </button>
                </td>
                <td>
                    <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#editTopicModal<?php echo $mytopics_list[$i]['topic_id']; ?>"><?php echo $this->getText('edit'); ?></button>
                </td>
            </tr>
            <?php
            }
        ?>
    </table>



</div>
<!-- GENERATE EDITTOPIC MODALS START -->
<?php
        
        for ($i = 0; $i < sizeof($mytopics_list); $i++) {
            
            ?>
    <div id="editTopicModal<?php echo $mytopics_list[$i]['topic_id']; ?>" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <?php echo $this->getText('editingTopic'); ?>
                    </h5>
                    <br>&nbsp;
                    <form method="post">
                        <style>
                            .btn-link {
                                border: none;
                                outline: none;
                                background: none;
                                cursor: pointer;
                                color: red;
                                padding: 0;
                                padding-top: 5px;
                                text-decoration: none;
                                font-family: inherit;
                                font-size: inherit;
                                margin: 0 auto;
                                text-align: center;
                            }
                        </style>
                        <input type="hidden" name="topicId" value="<?php echo $mytopics_list[$i]['topic_id']; ?>">
                        <div>
                            <button type="submit" name="intent" value="delete" class="btn-link"><h6>&#128465;</h6></button>
                        </div>
                    </form>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <form method="post">
                    <div class="modal-body">
                        <h5>
                            <?php echo $this->getText('topicTitle'); ?>
                        </h5>
                        <input type="text" value="<?php echo $mytopics_list[$i]['topic_title']; ?>" required class="form-control" name="topicTitle"><br>
                        <h5>
                            <?php echo $this->getText('topicRecommTech'); ?>
                        </h5>
                        <input type="text" value="<?php echo $mytopics_list[$i]['topic_recomm_tech']; ?>" required class="form-control" name="topicRecommTech"><br>
                        <h5>
                            <?php echo $this->getText('topicDescription'); ?>
                        </h5>
                        <textarea class="form-control" name="topicDescription" style="resize: none;" rows="10"><?php echo $mytopics_list[$i]['topic_desc']; ?></textarea><br>
                        <!-- FUTURE csatolmány feltöltés lehetősége -->
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="topicId" value="<?php echo $mytopics_list[$i]['topic_id']; ?>">
                        <button type="submit" name="intent" value="update" class="btn btn-primary"><?php echo $this->getText('saveChanges'); ?></button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                    </div>

                </form>
            </div>
        </div>
    </div>





    <?php
            
            }
        ?>
        <!-- GENERATE EDITTOPIC MODALS END -->




        <!-- GENERATE CHECKTOPIC MODALS START -->
        <?php for ($i = 0; $i < sizeof($mytopics_list); $i++) {
            $appliedstudents = $mytopics->getAppliedStudents($mytopics_list[$i]['topic_id']);
            ?>
        <div id="checkTopicModal<?php echo $mytopics_list[$i]['topic_id']; ?>" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <?php echo $this->getText('checkTopic'); ?>
                        </h5>
                        <br>&nbsp;
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <table id="table" class="table table-bordered table-striped table-condensed" style="#width:100%">
                        <tr>
                            <th style="width:60%;text-align:center;padding-top:16px;">
                                <?php echo $this->getText('studentName'); ?>
                            </th>
                            <th style="" colspan="">
                                <?php echo $this->getText('spec'); ?>
                            </th>
                            <th>
                                <?php echo $this->getText('accept'); ?>
                            </th>
                            <th>
                                <?php echo $this->getText('decline'); ?>
                            </th>
                        </tr>
                        <?php
                        
                        for ($j = 0; $j < sizeof($appliedstudents); $j++) {
                            ?>
                            <tr>

                                <td style="text-align: center;">
                                    <?php echo $appliedstudents[$j]['user_realname']; ?>
                                </td>
                                <td>
                                    <button id="btn" class="btn btn-sm btn-primary btn-block" onclick="location.href='/<?php echo $appliedstudents[$j]['student_upload'] ?>';"><?php echo $this->getText('showPreSpec'); ?></button>
                                </td>
                                <td>
                                    <form method="post" name="acceptTopicForm">
                                        <!-- button type="submit">&#10004;</button -->
                                        <a href="#" onclick="document.forms['acceptTopicForm'].submit(); return false;">&#10004;</a>
                                        <input type="hidden" name="intent" value="accept">
                                        <input type="hidden" name="topicId" value="<?php echo $mytopics_list[$i]['topic_id']; ?>">
                                        <input type="hidden" name="studentId" value="<?php echo $appliedstudents[$j]['user_id']  ?>">
                                    </form>
                                </td>
                                <td>

                                    <form method="post" name="declineTopicForm">
                                        <!-- button type="submit">&#10004;</button -->
                                        <a href="#" onclick="document.forms['declineTopicForm'].submit(); return false;">&#10060;</a>
                                        <input type="hidden" name="intent" value="decline">
                                        <input type="hidden" name="topicId" value="<?php echo $mytopics_list[$i]['topic_id']; ?>">
                                        <input type="hidden" name="studentId" value="<?php echo $appliedstudents[$j]['user_id']  ?>">
                                    </form>
                                </td>
                            </tr>
                            
                            <?php
                                
                            }
                        ?>
                    </table>
                    <?php if (sizeof($appliedstudents) == 0) {
                            echo "<h3 style='text-align:center;'>".$this->getText("noApplicantsYet")."</h3>";
                        } ?>
                </div>
            </div>
        </div>
        <?php
            
            }
        ?>
            <!-- GENERATE CHECKTOPIC MODALS END -->



            <div id="newTopicModal" class="modal fade">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">
                                <?php echo $this->getText('newTopic'); ?>
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="post">
                            <div class="modal-body">
                                <h5>
                                    <?php echo $this->getText('topicTitle'); ?>
                                </h5>
                                <input type="text" required class="form-control" name="topicTitle"><br>
                                <h5>
                                    <?php echo $this->getText('topicRecommTech'); ?>
                                </h5>
                                <input type="text" required class="form-control" name="topicRecommTech"><br>
                                <h5>
                                    <?php echo $this->getText('topicDescription'); ?>
                                </h5>
                                <textarea class="form-control" name="topicDescription" style="resize: none;" rows="10"></textarea><br>
                                <!-- FUTURE csatolmány feltöltés lehetősége -->
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="topicOwner" value="<?php echo $this->current_user->getUserId(); ?>">
                                <button type="submit" class="btn btn-primary"><?php echo $this->getText('newTopic'); ?></button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                            </div>
                            <input type="hidden" name="intent" value="insert">
                        </form>
                    </div>
                </div>
            </div>





            <script>
                function search() {
                    var input, filter, table, tr, td, i;
                    input = document.getElementById("searchText");
                    filter = input.value.toUpperCase();
                    table = document.getElementById("table");
                    tr = table.getElementsByTagName("tr");
                    for (i = 0; i < tr.length; i++) {
                        td0 = tr[i].getElementsByTagName("td")[0];

                        if (td0) {
                            if (td0.innerHTML.toUpperCase().indexOf(filter) > -1) {
                                tr[i].style.display = "";
                            } else {
                                tr[i].style.display = "none";
                            }
                        }
                    }
                }
            </script>