<style>
    #content {
        text-align: center;
        padding-top: 100px;
        padding-bottom: 50px;
        color: white;
        background: rgb(0, 140, 186);
    }

    h3 {
        line-height: 200%;
    }
</style>
<div id="content" class="container">
    <!--
    <h3>
        <table>
            <tr>
                <td><h3><?php echo $this->getText('myId'); ?>: <td>
                    <td><em><?php echo $this->current_user->getUserName(); ?></em></td></h3>
            </tr>
        <?php echo $this->getText('myName'); ?>: 
            <em><?php echo $this->current_user->getUserRealName(); ?></em>&#x270F;
        
        <?php echo $this->getText('myEmailAddr'); ?>: 
            <em><?php echo $this->current_user->getEmailAddr(); ?></em>&#x270F;
        
        <?php echo $this->getText('myPrivilege'); ?>: 
            <em><?php echo $this->getText($this->current_user->getPrivilege()); ?></em>
        </table>
    </h3>
    -->
    <table style="color: white; margin: auto;">
        <tr>
            <td style="text-align: right;">
                <h3>
                    <?php echo $this->getText('myId'); ?>:&nbsp;&nbsp;</h3>
                <td>
                    <td>
                        <h3><em><?php echo $this->current_user->getUserName(); ?></em></h3>
                    </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <h3>
                    <?php echo $this->getText('myName'); ?>:&nbsp;&nbsp;</h3>
                <td>
                    <td>
                        <h3><em><?php echo $this->current_user->getUserRealName(); ?></em></h3>
                    </td>

        </tr>
        <tr>
            <td style="text-align: right;">
                <h3>
                    <?php echo $this->getText('myEmailAddr'); ?>:&nbsp;&nbsp;</h3>
                <td>
                    <td>
                        <h3><em><?php echo $this->current_user->getEmailAddr(); ?></em></h3>
                    </td>
                    <td>
                        <h3>&nbsp;<button type="button" style="background: rgb(0,140,186);" class="btn" data-toggle="modal" data-target="#changeEmail"><h3>&#x270F;</h3></button></h3>
                    </td>
        </tr>
        <?php if ($this->current_user->getPrivilege() == "student") { ?>
        <tr>
            <td style="text-align: right;">
                <h3>
                    <?php echo $this->getText('myCloud'); ?>:&nbsp;&nbsp;</h3>
                <td>
                    <td>
                        <h3  style="display:block;text-overflow: ellipsis;width: 300px;overflow: hidden; white-space: nowrap;"><em><a style="color: white;" href="<?php echo Database::getCloud($this->current_user->getUserId()); ?>"><?php echo Database::getCloud($this->current_user->getUserId()); ?></a></em></h3>
                    </td>
                    <td>
                        <h3>&nbsp;<button type="button" style="background: rgb(0,140,186);" class="btn" data-toggle="modal" data-target="#changeCloud"><h3>&#x270F;</h3></button></h3>
                    </td>
        </tr>
        <?php } ?>
        <tr>
            <td style="text-align: right;">
                <h3>
                    <?php echo $this->getText('myPrivilege'); ?>:&nbsp;&nbsp;</h3>
                <td>
                    <td>
                        <h3><em><?php echo $this->current_user->getPrivilege(); ?></em></h3>
                    </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <h3>
                    <?php echo $this->getText('myPassword'); ?>:&nbsp;&nbsp;</h3>
                <td>
                    <td>
                        <h3> &#x1F510; </h3>
                    </td>
                    <td>
                        <h3>&nbsp;<button type="button" style="background: rgb(0,140,186);" class="btn" data-toggle="modal" data-target="#changePw"><h3>&#x270F;</h3></button></h3>
                    </td>
        </tr>
    </table>
</div>


<!-- START MODAL CHANGE EMAIL -->
<div class="modal fade" id="changeEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?php echo $this->getText("changeEmailAddr"); ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="email" required class="form-control" name="chEmail" value="<?php echo $this->current_user->getEmailAddr(); ?>"><br>
                    <p style="color: red; text-align: center;">
                        <?php echo $this->getText("logoutDisclaimer"); ?>
                    </p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="intent" value="changeEmail">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText("close"); ?></button>
                    <button type="submit" class="btn btn-primary"><?php echo $this->getText("saveChanges"); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODAL CHANGE EMAIL -->

<!-- START MODAL CHANGE CLOUD -->
<div class="modal fade" id="changeCloud" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?php echo $this->getText("changeCloud"); ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="text" required class="form-control" name="chCl" value="<?php echo Database::getCloud($this->current_user->getUserId()); ?>"><br>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="intent" value="changeCloud">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText("close"); ?></button>
                    <button type="submit" class="btn btn-primary"><?php echo $this->getText("saveChanges"); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODAL CHANGE CLOUD -->

<!-- START MODAL CHANGE PW -->
<div class="modal fade" id="changePw" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?php echo $this->getText("changePw"); ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <form method="post" >
                <div class="modal-body">
                    <input type="password" required onkeyup="checkPassword();" class="form-control" name="chPw" id="chPw" value="" placeholder="<?php echo $this->getText("newPw"); ?>"><br>
                    <input type="password" required onkeyup="checkPassword();" class="form-control" name="chPw2" id="chPw2" value="" placeholder="<?php echo $this->getText("newPwAgain"); ?>">
                </div>
                <p style="color: red; text-align: center;">
                        <?php echo $this->getText("logoutDisclaimer"); ?>
                    </p>
                <div class="modal-footer">
                    <input type="hidden" name="intent" value="changePw">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText("close"); ?></button>
                    <button type="submit" class="btn btn-primary" id="sButton" disabled><?php echo $this->getText("saveChanges"); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
function checkPassword() {
    if (document.getElementById("chPw").value != document.getElementById("chPw2").value)
    {
        document.getElementById("sButton").disabled = true;
    } else {
        document.getElementById("sButton").disabled = false;
    }
}
</script>
<!-- END MODAL CHANGE PW -->