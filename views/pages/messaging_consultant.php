<div id="content" class="container container-table">
    <form method="post">
        <select class="form-control" name="msgWith" onchange="this.form.submit()">
        <option value="sel"><?php echo $this->getText("selectFromList") ?></option>
        <?php
            for ($i = 0; $i < sizeof($myacceptedstudents); $i++) {
                ?>
                <option value="<?php echo $myacceptedstudents[$i]['user_id'] ?>" <?php if (isset($_POST['msgWith']) and $_POST['msgWith'] == $myacceptedstudents[$i]['user_id']) echo "selected"; ?> >
                <?php 
                $unreadFrom = Database::getUnreadFrom($this->current_user->getUserId(),$myacceptedstudents[$i]['user_id'])[0]['unread'];
                if ($unreadFrom > 0) {
                    echo $myacceptedstudents[$i]['user_realname']." - ".$myacceptedstudents[$i]['topic_title']." [".$unreadFrom."]"; 
                }else{
                    echo $myacceptedstudents[$i]['user_realname']." - ".$myacceptedstudents[$i]['topic_title']; 
                }
                
                
                    
                    ?>
                </option><?php
            }
        ?>
    </select>
    </form>
    <br>
    <?php if (isset($_POST['msgWith']) and $_POST['msgWith'] != "sel") { 
        if (Database::getCloud($_POST['msgWith']) != "") {
            ?>
    <h6 style="text-align:center;"><b><a href="<?php echo Database::getCloud($_POST['msgWith']); ?>"><?php echo $this->getText("cloudOpen") ?></a></b></h6>
    <h6 style="text-align:center;"><b><a href="<?php echo Database::getUpload2($_POST['msgWith']); ?>"><?php echo $this->getText("secondUploadOpen") ?></a></b></h6>
    <?php 
        } else {
            ?>
    <h6 style="text-align:center;"><b><?php echo $this->getText("cloudNotSetYet") ?></b></h6>
    <?php
        }
    } 
    ?>
        <br>
        <form method="post">
            <div style="width: 70%;margin: 0 auto;">
                <textarea class="form-control" style="" rows="5" name="message" required></textarea>
                <button class="btn btn-sm btn-primary btn-block" style="width:37.5%;float:right;" type="submit"><?php echo $this->getText("send") ?></button>
                <input type="hidden" name="intent" value="send">
                <input type="hidden" name="msgWith" value="<?php echo $_POST['msgWith']; ?>">
            </div>
        </form>
        <br>
        <table class="table">
            <?php //var_dump($pms); 
    if (isset($_POST['msgWith'])) {
        for ($i = 0; $i < sizeof($pms); $i++) {
            if ($pms[$i]['pm_recipient_user_id'] == $this->current_user->getUserId() and $pms[$i]['pm_sender_user_id'] == $_POST['msgWith']){
                ?>
            <tr>

                <td style="text-align:right;">
                    <div class="talk-bubble-right tri-right right-top">
                        <div class="talktext">
                            <h6><i><?php echo Database::getUserById($pms[$i]['pm_sender_user_id'])['user_realname'] ?></i></h6>
                            <i><?php echo $pms[$i]['pm_time']; ?></i>
                            <p>
                                <?php echo $pms[$i]['pm_message']; ?>
                            </p>
                        </div>
                    </div>

                </td>

            </tr>
            <?php
            }
            if ($pms[$i]['pm_sender_user_id'] == $this->current_user->getUserId()  and $pms[$i]['pm_recipient_user_id'] == $_POST['msgWith']){
                ?>
                <tr>
                    <td style="text-align:left;">
                        <div class="talk-bubble-left tri-right left-top">

                            <div class="talktext">
                                <h6><?php 
                
                            if (Database::isPmRead($pms[$i]['pm_id'])[0]['pm_unread'] == "0") {
                                echo "&#9745;";
                            }else{
                                echo "&#9744;";
                            }
                
                                echo "<i> -- ".$this->getText("me")."</i>"; ?></h6>
                                <i><?php echo $pms[$i]['pm_time']; ?></i>
                                <p>
                                    <?php echo $pms[$i]['pm_message']; ?>
                                </p>
                            </div>
                        </div>
                    </td>

                </tr>
                <?php
            }
        }
    }
    
    
    ?>
        </table>
</div>