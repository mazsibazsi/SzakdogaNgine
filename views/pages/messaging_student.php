<div id="content" class="container container-table">
    <h2 style="text-align:center;">
        <?php echo $myconsultant['topic_title']; ?>
    </h2>
    <h2 style="text-align: center;">
        <?php echo $myconsultant['user_realname']; ?>
    </h2><br>
    <form method="post">
        <div style="width: 70%;margin: 0 auto;">
            <textarea class="form-control" style="" rows="4" name="message" required></textarea>
            <button class="btn btn-sm btn-primary btn-block" style="width:37.5%;float:right;" type="submit"><?php echo $this->getText("send") ?></button>
            <input type="hidden" name="intent" value="send">
            <input type="hidden" name="msgWith" value="<?php echo $myconsultantid; ?>">
        </div>
    </form>
    <div class="talk-bubble tri-right left-top">
        <div class="talktext">
            <h6><i></i></h6>
            <p>

            </p>
        </div>
    </div>
    <br>

    <table class="table">
        <?php //var_dump($myconsultant); 
    if (isset($myconsultantid)) {
        for ($i = 0; $i < sizeof($pms); $i++) {
            if ($pms[$i]['pm_recipient_user_id'] == $this->current_user->getUserId() and $pms[$i]['pm_sender_user_id'] == $myconsultantid){
                ?>
        <tr>

            <td style="text-align:right;" >
                <div class="talk-bubble-right tri-right right-top">
                    <div class="talktext">
                        <h6><i><?php echo Database::getUserById($pms[$i]['pm_sender_user_id'])['user_realname'];
                            ?></i></h6>
                        <i><?php echo $pms[$i]['pm_time']; ?></i>
                        <p>
                            <?php echo $pms[$i]['pm_message']; ?>
                        </p>
                    </div>
                </div>

            </td>

        </tr>
        <?php
            }
            if ($pms[$i]['pm_sender_user_id'] == $this->current_user->getUserId()  and $pms[$i]['pm_recipient_user_id'] == $myconsultantid){
                ?>
            <tr >
                <td style="text-align:left;" >
                    <div class="talk-bubble-left tri-right left-top">

                        <div class="talktext">
                            <h6><?php
                
                            if (Database::isPmRead($pms[$i]['pm_id'])[0]['pm_unread'] == "0") {
                                echo "&#9745;";
                            }else{
                                echo "&#9744;";
                            }
                
                            echo "<i> -- ".$this->getText("me")."</i>"; ?></h6>
                            <i><?php echo $pms[$i]['pm_time']; ?></i>
                            <p>
                                <?php echo $pms[$i]['pm_message']; ?>
                            </p>
                        </div>
                        <?php 
                        
                        ?>
                    </div>
                </td>

            </tr>
            <?php
            }
        }
    }
    
    
    ?>
    </table>
</div>