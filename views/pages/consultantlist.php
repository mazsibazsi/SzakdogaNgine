<div id="content" class="container container-table">
    <h3 style="text-align: center;">
        <?php if (isset($message)) echo $message; ?>
    </h3>
    <input type="text" class="form-control" id="searchText" onkeyup="search()" placeholder="<?php echo $this->getText('search'); ?>">
    <table id="table" class="table table-bordered table-striped table-condensed" style="#width:100%">
        <tr>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('userName'); ?>
            </th>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('realName'); ?>
            </th>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('emailAddr'); ?>
            </th>
            <th style="">
                <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#newConsultantModal"><?php echo $this->getText('newConsultant'); ?></button>
            </th>
        </tr>
        <?php
        for ($i = 0; $i < sizeof($consultant_list); $i++) {
            ?>
            <tr>
                <td>
                    <?php echo $consultant_list[$i]['user_username']; ?>
                </td>

                <td>
                    <?php echo $consultant_list[$i]['user_realname']; ?>
                </td>

                <td>
                    <a href="mailto:<?php echo $consultant_list[$i]['user_email']; ?>">
                        <?php echo $consultant_list[$i]['user_email']; ?>
                    </a>
                </td>
                <td><button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#editConsultantModal<?php echo $consultant_list[$i]['user_id']; ?>"><?php echo $this->getText('edit'); ?></button></td>
            </tr>
            <?php
            }
        ?>
    </table>

    <?php
        for ($i = 0; $i < sizeof($consultant_list); $i++) {
            ?>
        <div id="editConsultantModal<?php echo $consultant_list[$i]['user_id']; ?>" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <?php echo $this->getText('editingConsultant'); ?>
                        </h5>
                        <br> &nbsp;

                        <!-- DELETE FORM BEGIN -->
                        <form method="post">
                            <style>
                                .btn-link {
                                    border: none;
                                    outline: none;
                                    background: none;
                                    cursor: pointer;
                                    color: red;
                                    padding: 0;
                                    padding-top: 5px;
                                    text-decoration: none;
                                    font-family: inherit;
                                    font-size: inherit;
                                    margin: 0 auto;
                                    text-align: center;
                                }
                            </style>
                            <input type="hidden" name="UserId" value="<?php echo $consultant_list[$i]['user_id']; ?>">
                            <div>
                                <button type="submit" name="intent" value="delete" class="btn-link"><h6>&#128465;</h6></button>
                            </div>
                        </form>
                        <!-- DELETE FORM END -->

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post">
                        <div class="modal-body">
                            <h5>
                                <?php echo $this->getText('realName'); ?>
                            </h5> <input type="text" required class="form-control" name="modRealName" value="<?php echo $consultant_list[$i]['user_realname']; ?>"><br>
                            <h5>
                                <?php echo $this->getText('userName'); ?>
                            </h5> <input type="text" required readonly class="form-control" name="modUserName" value="<?php echo $consultant_list[$i]['user_username']; ?>"><br>
                            <h5>
                                <?php echo $this->getText('emailAddr'); ?>
                            </h5> <input type="text" required class="form-control" name="modEmail" value="<?php echo $consultant_list[$i]['user_email']; ?>"><br>
                            <br>

                            <div class="form-group">
                                <div class="checkbox checbox-switch switch-primary">
                                    <label>
                                        <input type="checkbox" name="resetPw" value="do" />
                                        <span></span>
                                        <?php echo $this->getText("resetPw"); ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="UserId" value="<?php echo $consultant_list[$i]['user_id']; ?>">
                            <button type="submit" class="btn btn-primary"><?php echo $this->getText('saveChanges'); ?></button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                        </div>
                        <input type="hidden" name="intent" value="update">
                    </form>
                </div>
            </div>
        </div>
        <?php
            
            }
        ?>



</div>


<div id="newConsultantModal" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <?php echo $this->getText('newConsultant'); ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <h5>
                        <?php echo $this->getText('realName'); ?>
                    </h5> <input type="text" required class="form-control" name="newRealName"><br>
                    <h5>
                        <?php echo $this->getText('userName'); ?>
                    </h5> <input type="text" required class="form-control" name="newUserName"><br>
                    <h5>
                        <?php echo $this->getText('password'); ?>
                    </h5> <input type="text" placeholder="123" class="form-control" name="newUserPassword"><br>
                    <h5>
                        <?php echo $this->getText('emailAddr'); ?>
                    </h5> <input type="text" class="form-control" name="newEmail"><br>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo $this->getText('newConsultant'); ?></button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                </div>
                <input type="hidden" name="intent" value="insert">
            </form>
        </div>
    </div>
</div>





<script>
    function search() {
        // Declare variables
        var input, filter, table, tr, td, i;
        input = document.getElementById("searchText");
        filter = input.value.toUpperCase();
        table = document.getElementById("table");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td0 = tr[i].getElementsByTagName("td")[0];
            td1 = tr[i].getElementsByTagName("td")[1];
            td2 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            if (td0 || td1 || td2 || td3) {
                if (td0.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>