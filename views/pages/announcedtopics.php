<div id="content" class="container container-table">
    <h3 style="text-align: center;">
        <?php if (isset($message)) echo $message; ?>
    </h3>
    <input type="text" class="form-control" id="searchText" onkeyup="search()" placeholder="<?php echo $this->getText('search'); ?>">
    <table id="table" class="table table-bordered table-striped table-condensed" style="#width:100%">
        <tr>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('topicTitle'); ?>
            </th>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('topicAnnouncedBy'); ?>
            </th>
            <th>
                
            </th>
        </tr>
        <?php
        for ($i = 0; $i < sizeof($announcedtopics_list); $i++) {
            ?>
            <tr>

                <td style="text-align: center;">
                    <?php echo $announcedtopics_list[$i]['topic_title']; ?>
                </td>
                <td style="text-align: center;">
                    <?php
                            echo Database::getUserById($announcedtopics_list[$i]['topic_owner_user_id'])['user_realname'];
                        ?>
                </td>
                <td>
                    <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#showTopicModal<?php echo $announcedtopics_list[$i]['topic_id']; ?>"><?php echo $this->getText('show'); ?></button>
                </td>
            </tr>
            <?php
            }
        ?>
    </table>

    <?php
        for ($i = 0; $i < sizeof($announcedtopics_list); $i++) {
            ?>
        <div id="showTopicModal<?php echo $announcedtopics_list[$i]['topic_id']; ?>" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <?php echo $this->getText('watchingTopic'); ?>
                        </h5>
                        <br>&nbsp;


                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <h5>
                                <?php echo $this->getText('topicTitle'); ?>
                            </h5>
                            <h6><b><?php echo $announcedtopics_list[$i]['topic_title']; ?></b></h6>
                            <br>
                            <h5>
                                <?php echo $this->getText('topicAnnouncedBy'); ?>
                            </h5>
                            <h6><b><?php echo Database::getUserById($announcedtopics_list[$i]['topic_owner_user_id'])['user_realname']; ?></b></h6>
                            <br>
                            <h5>
                                <?php echo $this->getText('topicRecommTech'); ?>
                            </h5>
                            <p>
                                <?php echo $announcedtopics_list[$i]['topic_recomm_tech']; ?>
                            </p><br>
                            <br>
                            <h5>
                                <?php echo $this->getText('topicDescription'); ?>
                            </h5>
                            <p>
                                <?php echo $announcedtopics_list[$i]['topic_desc']; ?>
                            </p><br>
                            <?php
                                    if ($this->current_user->getPrivilege() == "student"){
                                ?>
                                <h5>
                                    <?php echo $this->getText('specUpload'); ?>
                                </h5>
                                <input type="file" name="fileToUpload" id="fileToUpload" required>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="intent" value="selectTopic">
                            <input type="hidden" name="topicId" value="<?php echo $announcedtopics_list[$i]['topic_id']; ?>">
                            <button type="submit" class="btn btn-primary"><?php echo $this->getText('selectTopic'); ?></button>
                            <?php
                                    }
                                ?>

                                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <?php
            
            }
        ?>



</div>






<script>
function search() {
  // Declare variables
  var input, filter, table, tr, td0, td1, i;
  input = document.getElementById("searchText");
  filter = input.value.toUpperCase();
  table = document.getElementById("table");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td0 = tr[i].getElementsByTagName("td")[0];
      td1 = tr[i].getElementsByTagName("td")[1];
      
    if (td0 || td1) {
        
      if (td0.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
          
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
    }

}
</script>