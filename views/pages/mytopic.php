<div id="content" class="container">
    <h3 style="text-align: center;">
        <?php if (isset($message)) echo $message; ?>
    </h3>
    <br>
    <u><h2 style="text-align: center;"><?php echo $this->getText('myTopic'); ?></h2></u>
    <?php //var_dump($mytopic); ?>

    <?php 
    
    if (!empty($mytopic)){
        ?>
    <br>
    <h1 style="text-align:center;">
        <?php echo $mytopic[0]['topic_title']; ?>
    </h1>
    <h2 style="text-align: center;">
        <?php echo $mytopic[0]['user_realname']; ?>
    </h2><br>
    <p style="text-align: center;">
        <?php echo $mytopic[0]['topic_desc']; ?>
    </p>
    <br>


    <?php 
        if ($mytopic[0]['student_accept'] == "1")
        {
            ?>

    <form method="post" enctype="multipart/form-data">
        <h5 style="text-align: center;">
            <?php echo $this->getText('secondUploadUpload'); ?>
        </h5>
        <input style="display: block;margin-left: auto;margin-right: auto;" type="file" name="fileToUpload" id="fileToUpload" required>
        <input type="hidden" name="topicId" value="<?php echo $mytopic[0]['topic_id']; ?>">
        <button style="width: 30%;display: block;margin-left: auto;margin-right: auto;" id="btn" class="btn btn-sm btn-primary btn-block" type="submit" name="intent" value="upload2"><?php echo $this->getText('upload'); ?></button>
    </form>
    <?php
        }
        ?>
        <ul class="list-group">
            <li class="list-group-item">
                <?php
                        if (!empty($mytopic[0]['student_upload'])){
                            echo "&#10004;";
                        }else{
                            echo "&#10060;";
                        }
                    ?>
                    &nbsp;
                    <?php echo "<a href=/".$mytopic[0]['student_upload'].">".$this->getText('firstUpload')."</a>"; ?>
            </li>
            <li class="list-group-item" >
                <?php
                        switch ($mytopic[0]['student_accept']){
                            case "1":
                                echo "                    &#10004;                     &nbsp;&nbsp;
";
                                echo $this->getText('topicApproved');
                                break;
                            case "-1":
                                echo "                    &#10060;                     &nbsp;&nbsp;
";
                                echo "<b>".$this->getText('topicDeclined')."</b>";
                                break;
                            default:
                                echo "                    &#10067;                     &nbsp;&nbsp;
";
                                echo $this->getText('topicAwaiting');
                                break;
                        }
                    ?>
            </li>
            <li class="list-group-item">
                <?php
                        if (!empty($mytopic[0]['student_upload_2'])){
                            echo "&#10004;";
                        }else{
                            echo "&#10060;";
                        }
                    ?>
                    &nbsp;
                    <?php echo "<a href=/".$mytopic[0]['student_upload_2'].">".$this->getText('secondUpload')."</a>"; ?>
            </li>
        </ul>
        <?php
    }else{
        ?>
            <h3 style="text-align: center; color: red;">
                <?php echo $this->getText('noTopicSelectedAndAcceptedYet'); ?>
            </h3>
            <?php
    }
    
    ?>
                <br>
                <h3>Határidők</h3>
                <table id="table" class="table table-bordered table-striped table-condensed" style="#width:100%">
                    <tr>
                        <th style="width:auto;text-align:center;padding-top:16px;">
                            <?php echo $this->getText('deadlineTitle'); ?>
                        </th>
                        <th style="width:auto;text-align:center;padding-top:16px;">
                            <?php echo $this->getText('deadlineDue'); ?>
                        </th>
                        <th style="">

                        </th>
                    </tr>
                    <?php
        for ($i = 0; $i < sizeof($deadlinelist); $i++) {
            ?>
                        <tr>

                            <td style="text-align: center;">
                                <?php echo $deadlinelist[$i]['deadline_title']; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php
                            echo $deadlinelist[$i]['deadline_due'];
                        ?>
                            </td>
                            <td>
                                <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#showDeadlineModal<?php echo $deadlinelist[$i]['deadline_id']; ?>"><?php echo $this->getText('show'); ?></button>
                            </td>
                        </tr>
                        <?php
            }
        ?>
                </table>

                <?php
        for ($i = 0; $i < sizeof($deadlinelist); $i++) {
            ?>
                    <div id="showDeadlineModal<?php echo $deadlinelist[$i]['deadline_id']; ?>" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">
                                        <?php echo $this->getText('deadlineDetails'); ?>
                                    </h5>
                                    <br>&nbsp;
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                                </div>
                                <div class="modal-body">
                                    <h5>
                                        <?php echo $this->getText('deadlineTitle'); ?>
                                    </h5>
                                    <h4><b><?php echo $deadlinelist[$i]['deadline_title']; ?></b></h4>
                                    <br>
                                    <h5>
                                        <?php echo $this->getText('deadlineDue'); ?>
                                    </h5>
                                    <h4>
                                        <?php echo $deadlinelist[$i]['deadline_due']; ?>
                                    </h4>
                                    <br>
                                    <h5>
                                        <?php echo $this->getText('deadlineDesc'); ?>
                                    </h5>
                                    <p>
                                        <?php echo $deadlinelist[$i]['deadline_desc']; ?>
                                    </p><br>
                                    <br>
                                </div>
                                <div class="modal-footer">

                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php
            
            }
        ?>

</div>