<div id="content" class="container container-table">
    <h3 style="text-align: center;">
        <?php if (isset($message)) echo $message; ?>
    </h3>
    <input type="text" class="form-control" id="searchText" onkeyup="search()" placeholder="<?php echo $this->getText('search'); ?>">
    <table id="table" class="table table-bordered table-striped table-condensed" style="#width:100%">
        <tr>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('deadlineTitle'); ?>
            </th>
            <th style="width:auto;text-align:center;padding-top:16px;">
                <?php echo $this->getText('deadlineDue'); ?>
            </th>
            <th style="">
                <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#newDeadlineModal"><?php echo $this->getText('new'); ?></button>
            </th>
        </tr>
        <?php
        for ($i = 0; $i < sizeof($deadlinelist); $i++) {
            ?>
            <tr>

                <td style="text-align: center;">
                    <?php echo $deadlinelist[$i]['deadline_title']; ?>
                </td>
                <td style="text-align: center;">
                    <?php
                            echo $deadlinelist[$i]['deadline_due'];
                        ?>
                </td>
                <td>
                    <button id="btn" class="btn btn-sm btn-primary btn-block" data-toggle="modal" data-target="#showDeadlineModal<?php echo $deadlinelist[$i]['deadline_id']; ?>"><?php echo $this->getText('show'); ?></button>
                </td>
            </tr>
            <?php
            }
        ?>
    </table>

    <?php
        for ($i = 0; $i < sizeof($deadlinelist); $i++) {
            ?>
        <div id="showDeadlineModal<?php echo $deadlinelist[$i]['deadline_id']; ?>" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <?php echo $this->getText('deadlineDetails'); ?>
                        </h5>
                        <br>&nbsp;
                        <form method="post">
                            <style>
                                .btn-link {
                                    border: none;
                                    outline: none;
                                    background: none;
                                    cursor: pointer;
                                    color: red;
                                    padding: 0;
                                    padding-top: 5px;
                                    text-decoration: none;
                                    font-family: inherit;
                                    font-size: inherit;
                                    margin: 0 auto;
                                    text-align: center;
                                }
                            </style>
                            <input type="hidden" name="dlId" value="<?php echo $deadlinelist[$i]['deadline_id']; ?>">
                            <div>
                                <button type="submit" name="intent" value="delete" class="btn-link"><h6>&#128465;</h6></button>
                            </div>
                        </form>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5>
                            <?php echo $this->getText('deadlineTitle'); ?>
                        </h5>
                        <h4><b><?php echo $deadlinelist[$i]['deadline_title']; ?></b></h4>
                        <br>
                        <h5>
                            <?php echo $this->getText('deadlineDue'); ?>
                        </h5>
                        <h4>
                            <?php echo $deadlinelist[$i]['deadline_due']; ?>
                        </h4>
                        <br>
                        <h5>
                            <?php echo $this->getText('deadlineDesc'); ?>
                        </h5>
                        <p>
                            <?php echo $deadlinelist[$i]['deadline_desc']; ?>
                        </p><br>
                        <br>
                    </div>
                    <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                    </div>

                </div>
            </div>
        </div>
        <?php
            
            }
        ?>


            <div id="newDeadlineModal" class="modal fade">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">
                                <?php echo $this->getText('deadlineDetails'); ?>
                            </h5>
                            <br>&nbsp;


                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <form method="post" class="form-control">
                            <div class="modal-body">
                                <h5>
                                    <?php echo $this->getText('deadlineTitle'); ?>
                                </h5>
                                <input type="text" name="dlTitle" class="form-control" required>
                                <br>
                                <h5>
                                    <?php echo $this->getText('deadlineDue'); ?>
                                </h5>
                                <h4>
                                    <input type="date" name="dlDue" class="form-control" required>
                                </h4>
                                <br>
                                <h5>
                                    <?php echo $this->getText('deadlineDesc'); ?>
                                </h5>
                                <p>
                                    <textarea name="dlDesc" class="form-control" required></textarea>

                                </p><br>
                                <br>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="intent" value="insert">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $this->getText('close'); ?></button>
                                <button type="submit" class="btn btn-primary"><?php echo $this->getText('send'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


</div>






<script>
    function search() {
        // Declare variables
        var input, filter, table, tr, td, i;
        input = document.getElementById("searchText");
        filter = input.value.toUpperCase();
        table = document.getElementById("table");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td0 = tr[i].getElementsByTagName("td")[0];
            td1 = tr[i].getElementsByTagName("td")[1];

            if (td0) {
                if (td0.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
            if (td1) {
                if (td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>