<style>
    /* bootstrap */
    body {
        padding-top: 120px;
        padding-bottom: 40px;
        background-color: #eee;
    }

    div.container {
        margin: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        min-width: 0;
    }

    .btn {
        outline: 0;
        border: none;
        border-top: none;
        border-bottom: none;
        border-left: none;
        border-right: none;
        box-shadow: inset 2px -3px rgba(0, 0, 0, 0.15);
        background-color: midnightblue;
    }

    .btn:focus {
        outline: 0;
        -webkit-outline: 0;
        -moz-outline: 0;
    }

    .fullscreen_bg {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background-size: cover;
        background-position: 50% 50%;
        background-color: #008cba;
        background-repeat: repeat;
        min-width: 0;
    }

    .form-signin {

        padding: 15px;
        margin: 0 auto;
        #margin-top: 25%;
    }

    .form-w {
        max-width: 500px;
        padding: 15px;
        margin: 0 auto;
    }

    .form-signin .form-signin-heading,
    .form-signin {
        margin-bottom: 10px;
    }

    .form-signin .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .form-signin .form-control:focus {
        z-index: 2;
    }

    .form-signin input[type="text"] {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
        border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: none;
        border-left-style: solid;
        border-color: #000;
    }

    .form-signin input[type="password"] {
        /*margin-bottom: 10px;*/
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        margin-bottom: 0;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
        border-top-style: none;
        border-right-style: solid;
        /*border-bottom-style: solid;*/
        border-bottom-style: none;
        border-left-style: solid;
        border-color: rgb(0, 0, 0);
        border-top: 1px solid rgba(0, 0, 0, 0.08);
    }

    .form-signin select {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-top-style: none;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;
        border-color: rgb(0, 0, 0);
        border-top: 1px solid rgba(0, 0, 0, 0.08);
        font-size: 50px;

    }

    .form-signin-heading {
        color: white;
        text-align: center;
        text-shadow: 0 2px 2px rgba(0, 0, 0, 0.5);
    }

    h1 {
        color: white;
    }
</style>

<div id="fullscreen_bg" class="fullscreen_bg">

    <div class="container">
        <title>SzakdogaNgine Login</title>
        <form class="form-signin" method="post" action="/auth/login">
           <?php 
            if (AppConfig::get("loginLogo") != "/res/"){
            ?><img style="margin: 0 auto;display: block;max-width: 25%;" src="<?php echo AppConfig::get("loginLogo"); ?>"><?php
            }
            ?>
            <h2 id="title1" class="form-signin-heading" style="text-overflow: ellipsis;white-space:normal;font-size: 2.5vw;"><?php echo AppConfig::get("schoolTitle"); ?></h2>
            <div class="form-w">

                <h1 id="title2" class="form-signin-heading" style="text-overflow: ellipsis;white-space:normal;font-size: 2.0vw; ">szakmai záródolgozat konzultációs rendszere</h1>
                <input type="text" class="form-control" name="l_id" placeholder="Azonosító"  required="" autofocus="" >
                <input type="password" class="form-control" name="l_pwd" placeholder="Jelszó" >
                <select name="l_lang" class="form-control input-lg" style="height: 50px;" id="l_lang" onChange="selectOnChange()">
                    <option value="hun">Magyar</option>
                    <?php 
                    if (!strcmp(AppConfig::get("ger"),"true")) { ?>
                    <option value="deu">Deutsch</option>
                    <?php } 
                    if (!strcmp(AppConfig::get("eng"),"true")) { ?>
                    <option value="eng">English</option>
                    <?php } ?>
                    <!-- option value="esp">Español</option>
                    <option value="jap">日本語</option -->
                </select>
                <input type="hidden" name="l_intent" value="true">
                <button id="btn" class="btn btn-lg btn-primary btn-block" type="submit">
                    Bejelentkezés
                </button>
                <h3>
                    <?php 
                if (isset($_POST['errormsg'])) {
                    echo $_POST['errormsg'];
                }
                ?>
                </h3>
            </div>

        </form>

    </div>
    <a href="https://gitlab.com/mazsibazsi/SzakdogaNgine">
                <img style="max-width: 5%; position: absolute; bottom:0;right: 0;" src="/res/gitlab.png">
            </a>
</div>

<script>
    var name = "<?php echo AppConfig::get("pageTitle"); ?>";
    name = " - " + name;
    document.title = "Login" + name;
    //if select option is changed, run this:
    function selectOnChange() {
        switch (document.getElementById("l_lang").selectedIndex) {
            case 0:
                document.getElementsByName("l_id")[0].placeholder = "Azonosító szám";
                document.getElementsByName("l_pwd")[0].placeholder = "Jelszó";
                document.getElementById("btn").innerHTML = "Bejelentkezés";
                document.getElementById("title1").innerHTML = "<?php echo AppConfig::get("schoolTitle"); ?>";
                document.getElementById("title2").innerHTML = "szakmai záródolgozat konzultációs rendszere";
                break;
            case 1:
                document.getElementsByName("l_id")[0].placeholder = "Kennzeichennummer";
                document.getElementsByName("l_pwd")[0].placeholder = "Kennwort";
                document.getElementById("btn").innerHTML = "Anmelden";
                document.getElementById("title1").innerHTML = "<?php echo AppConfig::get("schoolTitleGer"); ?>";
                document.getElementById("title2").innerHTML = "Beratungssystem für berufliche Schließarbeiten";
                break;
            case 2:
                document.getElementsByName("l_id")[0].placeholder = "Identification number";
                document.getElementsByName("l_pwd")[0].placeholder = "Password";
                document.getElementById("btn").innerHTML = "Log in";
                document.getElementById("title1").innerHTML = "<?php echo AppConfig::get("schoolTitleEng"); ?>";
                document.getElementById("title2").innerHTML = "consultation system for final dissertations";
                break;
                /*case 3:
			document.getElementsByName("l_id")[0].placeholder = "Número de identificación";
			document.getElementsByName("l_pwd")[0].placeholder = "Contraseña";
			document.getElementById("btn").innerHTML = "Iniciar sesión";
			document.getElementById("title1").innerHTML = "<abbr title='Budapest Centros de Formación Técnica Profesional'>BCFTP</abbr> Petrik Lajos Escuela Bilingüe Vocacional de Química, Protección del Medioambiente y Tecnología Informática";
			document.getElementById("title2").innerHTML = "sistema de consultas para disertaciones finales";
			break;
        case 4:
			document.getElementsByName("l_id")[0].placeholder = "ID番号";
			document.getElementsByName("l_pwd")[0].placeholder = "パスワード";
			document.getElementById("btn").innerHTML = "ログイン";
			document.getElementById("title1").innerHTML = "ブダペスト技術職業訓練センター<br>ペロリくラヨシュ 環境保護と情報技術のバイリンガル職業学校";
			document.getElementById("title2").innerHTML = "最終学位論文の<br>相談システム";
			break;*/
        }
    };
</script>