<div id="content" class="container container-table">
    <form method="post">
        <select class="form-control" name="selectedStudent" onchange="this.form.submit()">
        <option value="sel"><?php echo $this->getText("selectFromList") ?></option>
        <?php
            for ($i = 0; $i < sizeof($myacceptedstudents); $i++) {
                ?>
                <option value="<?php echo $myacceptedstudents[$i]['user_id'] ?>" <?php if (isset($_POST['selectedStudent']) and $_POST['selectedStudent'] == $myacceptedstudents[$i]['user_id']) echo "selected"; ?> >
                <?php 
                echo $myacceptedstudents[$i]['user_realname']." - ".$myacceptedstudents[$i]['topic_title']; 
                
                
                    
                ?>
                </option><?php
            }
        ?>
    </select>
    </form>
    <form method="post">
        <?php if (isset($_POST['selectedStudent']) and $_POST['selectedStudent'] != "sel") { 
        ?>
        <br>
        <input class="form-control" type="text" name="finalTitle" placeholder="<?php echo $this->getText("finalTitle"); ?>" value="<?php echo $selectedStudent['protocol_final_title']; ?>"><br>
        <div class="panel-group" id="accordion">
             <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    <button class="btn btn-block" style="text-align:center;" id="buttonOne" onclick="buttonPrimCh('buttonOne')"><?php echo "1. ".$this->getText("consultation");
                        if (!empty($selectedStudent['protocol_1_pos']) and
                           !empty($selectedStudent['protocol_1_mis']) and
                           !empty($selectedStudent['protocol_1_rec']) and
                           !empty($selectedStudent['protocol_1_prog_ready']) and
                           !empty($selectedStudent['protocol_1_docu_ready']) ) 
                        echo " &#10004;";
                        ?></button></a>
                  </h4>
                </div>
                
                <div id="collapse1" class="panel-collapse collapse in">
                  <div class="panel-body">
                        
                        <h4 style="text-align:center;"><?php echo $this->getText("positives") ?></h4>
                        <textarea class="form-control" name="1_pos" placeholder="<?php echo $this->getText("positives") ?>"><?php echo $selectedStudent['protocol_1_pos']; ?></textarea><br>
                        <h4 style="text-align:center;"><?php echo $this->getText("shortcomings") ?></h4>
                        <textarea class="form-control" name="1_mis" placeholder="<?php echo $this->getText("shortcomings") ?>"><?php echo $selectedStudent['protocol_1_mis']; ?></textarea><br>
                        <h4 style="text-align:center;"><?php echo $this->getText("recommendations") ?></h4>
                        <textarea class="form-control" name="1_rec" placeholder="<?php echo $this->getText("recommendations") ?>"><?php echo $selectedStudent['protocol_1_rec']; ?></textarea><br>
                        <table class="table" >
                            <tr>
                                <td> 
                                    <h5><?php echo $this->getText("completionProg") ?>: (%)</h5>&nbsp;
                                </td>
                                <td>
                                    <input name="1_prog_comp" class="form-control" type="number" min="0" max="100" value="<?php echo $selectedStudent['protocol_1_prog_ready']; ?>">
                                </td>
                                <td>
                                    <h5><?php echo $this->getText("completionDocs") ?>: (%)</h5> 
                                </td>
                                <td>
                                    <input name="1_docs_comp" class="form-control" type="number" min="0" max="100" value="<?php echo $selectedStudent['protocol_1_docu_ready']; ?>">
                                </td>
                                <td>
                                    <button class="btn btn-primary" type="submit"><?php echo $this->getText("send"); ?></button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"><h5 style="text-align:center;"><?php echo $this->getText("minExpectation"); ?> 30%</h5></td>
                            </tr>
                      </table>
                      <br>
                  </div>
                </div>
              </div>
              
               <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    <button class="btn btn-block" style="text-align:center;" id="buttonTwo" onclick="buttonPrimCh('buttonTwo')"><?php echo "2. ".$this->getText("consultation") ;
                        if (!empty($selectedStudent['protocol_2_pos']) and
                           !empty($selectedStudent['protocol_2_mis']) and
                           !empty($selectedStudent['protocol_2_rec']) and
                           !empty($selectedStudent['protocol_2_prog_ready']) and
                           !empty($selectedStudent['protocol_2_docu_ready']) ) 
                        echo " &#10004;";
                        ?></button></a>
                  </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse in">
                  <div class="panel-body">
                        
                        <h4 style="text-align:center;"><?php echo $this->getText("positives") ?></h4>
                        <textarea class="form-control" name="2_pos" placeholder="<?php echo $this->getText("positives") ?>"><?php echo $selectedStudent['protocol_2_pos']; ?></textarea><br>
                        <h4 style="text-align:center;"><?php echo $this->getText("shortcomings") ?></h4>
                        <textarea class="form-control" name="2_mis" placeholder="<?php echo $this->getText("shortcomings") ?>"><?php echo $selectedStudent['protocol_2_mis']; ?></textarea><br>
                        <h4 style="text-align:center;"><?php echo $this->getText("recommendations") ?></h4>
                        <textarea class="form-control" name="2_rec" placeholder="<?php echo $this->getText("recommendations") ?>"><?php echo $selectedStudent['protocol_2_rec']; ?></textarea><br>
                        <table class="table" >
                            <tr>
                                <td> 
                                    <h5><?php echo $this->getText("completionProg") ?>: (%)</h5>&nbsp;
                                </td>
                                <td>
                                    <input name="2_prog_comp" class="form-control" type="number" min="0" max="100" value="<?php echo $selectedStudent['protocol_2_prog_ready']; ?>">
                                </td>
                                <td>
                                    <h5><?php echo $this->getText("completionDocs") ?>: (%)</h5> 
                                </td>
                                <td>
                                    <input name="2_docs_comp" class="form-control" type="number" min="0" max="100" value="<?php echo $selectedStudent['protocol_2_docu_ready']; ?>">
                                </td>
                                <td>
                                    <button class="btn btn-primary" type="submit"><?php echo $this->getText("send"); ?></button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"><h5 style="text-align:center;"><?php echo $this->getText("minExpectation"); ?> 60%</h5></td>
                            </tr>
                      </table>
                      <br>
                  </div>
                </div>
              </div>
              
               <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                    <button class="btn btn-block" style="text-align:center;" id="buttonThree" onclick="buttonPrimCh('buttonThree')"><?php echo "3. ".$this->getText("consultation");
                        if (!empty($selectedStudent['protocol_3_pos']) and
                           !empty($selectedStudent['protocol_3_mis']) and
                           !empty($selectedStudent['protocol_3_rec']) and
                           !empty($selectedStudent['protocol_3_prog_ready']) and
                           !empty($selectedStudent['protocol_3_docu_ready']) ) 
                        echo " &#10004;";
                        ?></button></a>
                  </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse in">
                  <div class="panel-body">
                        
                        <h4 style="text-align:center;"><?php echo $this->getText("positives") ?></h4>
                        <textarea class="form-control" name="3_pos" placeholder="<?php echo $this->getText("positives") ?>"><?php echo $selectedStudent['protocol_3_pos']; ?></textarea><br>
                        <h4 style="text-align:center;"><?php echo $this->getText("shortcomings") ?></h4>
                        <textarea class="form-control" name="3_mis" placeholder="<?php echo $this->getText("shortcomings") ?>"><?php echo $selectedStudent['protocol_3_mis']; ?></textarea><br>
                        <h4 style="text-align:center;"><?php echo $this->getText("recommendations") ?></h4>
                        <textarea class="form-control" name="3_rec" placeholder="<?php echo $this->getText("recommendations") ?>"><?php echo $selectedStudent['protocol_3_rec']; ?></textarea><br>
                                                <table class="table" >
                            <tr>
                                <td> 
                                    <h5><?php echo $this->getText("completionProg") ?>: (%)</h5>&nbsp;
                                </td>
                                <td>
                                    <input name="3_prog_comp" class="form-control" type="number" min="0" max="100" value="<?php echo $selectedStudent['protocol_3_prog_ready']; ?>">
                                </td>
                                <td>
                                    <h5><?php echo $this->getText("completionDocs") ?>: (%)</h5> 
                                </td>
                                <td>
                                    <input name="3_docs_comp" class="form-control" type="number" min="0" max="100" value="<?php echo $selectedStudent['protocol_3_docu_ready']; ?>">
                                </td>
                                <td>
                                    <button class="btn btn-primary" type="submit"><?php echo $this->getText("send"); ?></button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"><h5 style="text-align:center;"><?php echo $this->getText("minExpectation"); ?> 90%</h5></td>
                            </tr>
                      </table>
                      <br>
                  </div>
                </div>
              </div>
        </div>
        
        <input type="hidden" name="selectedStudent" value="<?php echo $selectedStudent['protocol_student_user_id']; ?>" >
        <input type="hidden" name="intent" value="update">
    </form>
    
                  <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                   <form method="post">
                   
                    
                    <button class="btn btn-block <?php
                                   if ($selectedStudent['protocol_exam_ready'] != 0) {
                                       echo "btn-primary";
                                   }
                                   ?>" style="text-align:center;" id="buttonFour" type="submit">
                                   <?php 
                                    if ($selectedStudent['protocol_exam_ready'] != 0) {
                                       
                                        echo $this->getText("examReady");
                                   }else{
                                        echo $this->getText("examNotReady");
                                    }
    
                                ?>
                                   </button>
                          <input type="hidden" name="val" value="<?php 
                                                                 echo $selectedStudent['protocol_exam_ready'] ;
                                                                 ?>">
                         <input type="hidden" name="selectedStudent" value="<?php echo $selectedStudent['protocol_student_user_id']; ?>">
                      </form>
                  </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse in">
                </div>
              </div>
</div>
<?php
        }
        ?>

<script>
function buttonPrimCh(btnid) {
    switch (btnid) {
        case "buttonOne":
            if (document.getElementById("buttonOne").classList.contains('btn-primary')) {
                document.getElementById("buttonOne").classList.remove('btn-primary');
            }else{
                document.getElementById("buttonOne").classList.add('btn-primary');
            }
            
            document.getElementById("buttonTwo").classList.remove('btn-primary');
            document.getElementById("buttonThree").classList.remove('btn-primary');
            document.getElementById('buttonOne').disabled = true
            setTimeout(function(){document.getElementById('buttonOne').disabled = false },2000);
            
            break;
        case "buttonTwo":
            document.getElementById("buttonOne").classList.remove('btn-primary');
            if (document.getElementById("buttonTwo").classList.contains('btn-primary')) {
                document.getElementById("buttonTwo").classList.remove('btn-primary');
            }else{
                document.getElementById("buttonTwo").classList.add('btn-primary');
            }
            document.getElementById("buttonThree").classList.remove('btn-primary');
            
            document.getElementById('buttonTwo').disabled = true
            setTimeout(function(){document.getElementById('buttonTwo').disabled = false },2000);
            break;
        case "buttonThree":
            document.getElementById("buttonOne").classList.remove('btn-primary');
            document.getElementById("buttonTwo").classList.remove('btn-primary');
            if (document.getElementById("buttonThree").classList.contains('btn-primary')) {
                document.getElementById("buttonThree").classList.remove('btn-primary');
            }else{
                document.getElementById("buttonThree").classList.add('btn-primary');
            }
            
            document.getElementById('buttonThree').disabled = true
            setTimeout(function(){document.getElementById('buttonThree').disabled = false },2000);
            
            break;
    }
}    
</script>