<style>
    .navbar-fixed-top,
    .navbar-fixed-bottom {
        position: fixed;
        /* <-- Look here */
        right: 0;
        left: 0;
        z-index: 1030;
    }
</style>
<nav id="navmenu" class="navbar navbar-expand-lg navbar-dark bg-primary navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="/pages/home"><img src="<?php echo AppConfig::get("homeLogo"); ?>" style="max-width: 50%;" alt="<?php echo AppConfig::get("pageTitle"); ?>"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?php if ($_GET['action'] == "mytopics" or $_GET['action'] == "home") echo "active "; ?>">
                    <a class="nav-link" href="/pages/mytopics"><?php echo $this->getText('myTopics'); ?><span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item <?php if ($_GET['action'] == "announcedtopics") echo "active "; ?>">
                    <a class="nav-link" href="/pages/announcedtopics"><?php echo $this->getText('announcedTopics'); ?></a>
                </li>
                <li class="nav-item <?php if ($_GET['action'] == "messaging") echo "active "; ?>">
                    <a class="nav-link" href="/pages/messaging"><?php 
                        echo $this->getText('messaging'); 
                        $unread = Database::getUnread($this->current_user->getUserId())[0]['unread'];
                        if ($unread > 0) {
                            echo " [<span style='color:white;'>".$unread."</span>]";
                        }
                        
                        ?></a>
                </li>
                <li class="nav-item <?php if ($_GET['action'] == "summary") echo "active "; ?>">
                    <a class="nav-link" href="/pages/summary">
                        <?php echo $this->getText('summary'); ?>
                    </a>
                </li>
                <li class="nav-item <?php if ($_GET['action'] == "protocol") echo "active"; ?>">
                    <a class="nav-link" href="/pages/protocol"><?php echo $this->getText('protocol'); ?></a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="nav-link" style="color: white" href="/pages/manage"><?php echo $this->current_user->getUserRealName(); ?></a>
                </li>
                <li>
                    <a class="nav-link" href="/auth/logout"><?php echo $this->getText('logout'); ?></a>
                </li>
            </ul>

        </div>
    </div>
</nav>