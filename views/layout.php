<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/jquery.min.css">
    <link rel="stylesheet" href="/css/popper.min.css">
    <link rel="stylesheet" href="/css/custom.css">
    <title></title>
    <script>
        var name = "<?php echo AppConfig::get("pageTitle"); ?>";
        name = " - " + name;
    </script>
</head>

<body>
    <img id="load" src="/res/loading.gif" style="
            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            border: 2px solid rgb(0,140,186);
            background: white;
            border-radius: 8px;">
    <script src="/scripts/jquery-3.2.1.slim.min.js"></script>
    <script src="/scripts/popper.min.js"></script>
    <script src="/scripts/bootstrap.min.js"></script>
    <?php
		require_once("routes.php");
		?>
    <script>
        window.onload = function() {
            document.getElementById("load").removeAttribute("style");
            document.getElementById("load").setAttribute("style", "display:block;");
            document.getElementById("load").removeAttribute("src");
        }
    </script>
</body>

</html>