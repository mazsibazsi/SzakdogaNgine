<?php

class Topics {
    
    private $topic_list;
    
        public function __construct() {
            
        }
    
        public function getMyTopics($userid) {
            $this->topic_list = Database::getMyTopics($userid);
        }
    
        public function getAllTopics() {
            $this->topic_list = Database::getAllTopics();
        }
    
        public function getList() {
            return $this->topic_list;
        }
    
        public function getAppliedStudents($topicid) {
            return Database::getAppliedStudentsForTopic($topicid);
        }
    
}


?>