<?php

class AppConfig {
    
    private static $configs;
    private static $exceptions = array("serveraddr","username","password","dbname");
        
    public function __construct() {
        $myFile = __DIR__."/../config.cfg";
        
        if (file_exists($myFile)) {
            $fh = fopen($myFile, 'r');

            $theData = fread($fh, filesize($myFile));
            $assoc_array = array();
            $my_array = explode("\n", $theData);
            foreach($my_array as $line)
            {
                $tmp = explode("=", $line);
                $assoc_array[$tmp[0]] = $tmp[1];
            }
            fclose($fh);
            self::$configs = $assoc_array;

            //error_reporting(0);
            //ini_set('display_errors', 0);
        } else {
            header("Location: /install.php");
        }
        
        
    }
    
    public static function get($text){
        if (in_array($text,self::$exceptions))
        return trim(self::$configs[$text]);
        return rtrim(self::$configs[$text]);
    }
    
}

?>