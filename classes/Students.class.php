<?php
    class Students {
        
        private $student_list;
        
        public function __construct() {
            $this->student_list = Database::getAllByPrivilege("student");
            //var_dump($this->student_list);
        }
        
        public function getList() {
            return $this->student_list;
        }
        
        public function getById($id) {
            return $this->student_list[$id];
        }
        
    }
?>