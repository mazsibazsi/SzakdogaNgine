<?php
    class Consultants {
        
        private $consultant_list;
        
        public function __construct() {
            $this->consultant_list = Database::getAllByPrivilege("consultant");
            //var_dump($this->student_list);
        }
        
        public function getList() {
            return $this->consultant_list;
        }
        
        public function getById($id) {
            return $this->consultant_list[$id];
        }
        
    }
?>