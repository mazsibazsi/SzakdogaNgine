<?php

class User {
	private $user_id;
    private $user_username;
	private $user_email;
	private $user_realname;
	private $user_pass;
	private $user_session_id;
	private $user_privilege;

	public function __construct ($user_array) {
		$this->user_id = $user_array['user_id'];
        $this->user_username = $user_array['user_username'];
		$this->user_email = $user_array['user_email'];
		$this->user_realname = $user_array['user_realname'];
		$this->user_pass = $user_array['user_pass'];
		$this->user_session_id = $user_array['user_session_id'];
		$this->user_privilege = $user_array['user_privilege'];
	}

	public function generateSessionId() {
        $this->user_session_id = base64_encode(sha1($this->user_id).sha1(date("Y-m-d h:i:s")));
        
		return $this->user_session_id;
	}
    
    public function getUserId() {
        return $this->user_id;
    }

	public function getPass() {
		return $this->user_pass;
	}
    
    public function getSessionId() {
        return $this->user_session_id;
    }
    
    public function getPrivilege() {
        return $this->user_privilege;
    }
    
    public function getUserRealName() {
        return $this->user_realname;
    }
    
    public function getUserName() {
        return $this->user_username;
    }
    
    public function getEmailAddr() {
        return $this->user_email;
    }
}

?>