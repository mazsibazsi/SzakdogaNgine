<?php

class Lang {
    
    private $langSelected;
    private $lang;
    
    public function __construct($sel_lang) {
        $this->langSelected = $sel_lang;
        $myFile = __DIR__."/../lang/".$this->langSelected.".txt";
        //echo $myFile;
        $fh = fopen($myFile, 'r');
        
        $theData = fread($fh, filesize($myFile));
        $assoc_array = array();
        $my_array = explode("\n", $theData);
        foreach($my_array as $line)
        {
            $tmp = explode("=", $line);
            $assoc_array[$tmp[0]] = $tmp[1];
        }
        fclose($fh);
        $this->lang = $assoc_array;
    }
    
    public function get($text){
        return $this->lang[$text];
    }
    
}

?>