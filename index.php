<?php

require_once('classes/AppConfig.php');

$appconfig = new AppConfig();

session_start();

require_once('connection.php');
require_once('classes/User.class.php');
require_once('classes/Lang.class.php');



$db = new Database();

if (isset($_GET['controller']) && isset($_GET['action'])) {
	$controller = $_GET['controller'];
	$action     = $_GET['action'];
} else if (!isset($_SESSION['current_user'])){
	$controller = 'pages';
	$action     = 'login';
} else {
	$controller = 'pages';
	$action     = 'home';
}

require_once('views/layout.php');
?>