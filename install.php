<?php
if (isset($_POST['install'])) {
    

    $sql="
    
CREATE TABLE `deadlines` (
  `deadline_id` int(11) NOT NULL,
  `deadline_title` varchar(512) COLLATE utf8_hungarian_ci NOT NULL,
  `deadline_due` date NOT NULL,
  `deadline_desc` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

CREATE TABLE `pms` (
  `pm_id` int(11) UNSIGNED NOT NULL,
  `pm_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pm_recipient_user_id` int(11) DEFAULT NULL,
  `pm_sender_user_id` int(11) DEFAULT NULL,
  `pm_message` longtext CHARACTER SET utf8,
  `pm_unread` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

CREATE TABLE `protocols` (
  `protocol_student_user_id` int(11) NOT NULL,
  `protocol_final_title` varchar(256) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `protocol_1_pos` text COLLATE utf8_hungarian_ci,
  `protocol_1_mis` text COLLATE utf8_hungarian_ci,
  `protocol_1_rec` text COLLATE utf8_hungarian_ci,
  `protocol_1_prog_ready` int(3) DEFAULT NULL,
  `protocol_1_docu_ready` int(3) DEFAULT NULL,
  `protocol_2_pos` text COLLATE utf8_hungarian_ci,
  `protocol_2_mis` text COLLATE utf8_hungarian_ci,
  `protocol_2_rec` text COLLATE utf8_hungarian_ci,
  `protocol_2_prog_ready` int(3) DEFAULT NULL,
  `protocol_2_docu_ready` int(3) DEFAULT NULL,
  `protocol_3_pos` text COLLATE utf8_hungarian_ci,
  `protocol_3_mis` text COLLATE utf8_hungarian_ci,
  `protocol_3_rec` text COLLATE utf8_hungarian_ci,
  `protocol_3_prog_ready` int(3) DEFAULT NULL,
  `protocol_3_docu_ready` int(3) DEFAULT NULL,
  `protocol_exam_ready` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

CREATE TABLE `students` (
  `student_user_id` int(11) NOT NULL,
  `student_class` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `student_topic_id` int(11) DEFAULT NULL,
  `student_upload` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `student_accept` tinyint(1) DEFAULT '0',
  `student_upload_2` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `student_cloud` varchar(256) COLLATE utf8_hungarian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

CREATE TABLE `topics` (
  `topic_id` int(11) NOT NULL,
  `topic_title` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `topic_recomm_tech` text CHARACTER SET utf8 NOT NULL,
  `topic_desc` longtext CHARACTER SET utf8,
  `topic_owner_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(45) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_realname` varchar(128) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_email` varchar(128) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_pass` varchar(64) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_session_id` varchar(128) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_privilege` enum('admin','consultant','student') COLLATE utf8_hungarian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

INSERT INTO `users` (`user_id`, `user_username`, `user_realname`, `user_email`, `user_pass`, `user_session_id`, `user_privilege`) VALUES
(1, 'admin', 'Admin', 'admin@mazsibazsi.moe', '".password_hash("123",PASSWORD_DEFAULT)."', NULL, 'admin');
    
  ALTER TABLE `deadlines`
  ADD PRIMARY KEY (`deadline_id`);
  
ALTER TABLE `pms`
  ADD PRIMARY KEY (`pm_id`),
  ADD KEY `pm_recipient_user_id` (`pm_recipient_user_id`),
  ADD KEY `pm_sender_user_id` (`pm_sender_user_id`);
  
  ALTER TABLE `students`
  ADD PRIMARY KEY (`student_user_id`),
  ADD KEY `fk_students_users_idx` (`student_user_id`),
  ADD KEY `fk_students_topics1` (`student_topic_id`);
  
  ALTER TABLE `topics`
  ADD PRIMARY KEY (`topic_id`),
  ADD KEY `fk_topics_users1_idx` (`topic_owner_user_id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_username_UNIQUE` (`user_username`);
  
  ALTER TABLE `protocols`
  ADD PRIMARY KEY (`protocol_student_user_id`);
  
  ALTER TABLE `deadlines`
  MODIFY `deadline_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
  
  ALTER TABLE `pms`
  MODIFY `pm_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
  
  ALTER TABLE `topics`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
  
  ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
  
  ALTER TABLE `protocols`
  ADD CONSTRAINT `protocols_ibfk_1` FOREIGN KEY (`protocol_student_user_id`) REFERENCES `students` (`student_user_id`);
    ";
    
    $servername = $_POST['serveraddr'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $dbname = $_POST['dbname'];
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    
    if (mysqli_multi_query($conn, $sql)) {
    echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);

    
    $target_dir = "res/";
    $target_file = $target_dir . basename($_FILES["loginLogo"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["loginLogo"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    
    if ($_FILES["loginLogo"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    
    } else {
        if (move_uploaded_file($_FILES["loginLogo"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["loginLogo"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
    $loginLogo = basename( $_FILES["loginLogo"]["name"]);
    echo "<br>";
    $target_dir = "res/";
    $target_file = $target_dir . basename($_FILES["homeLogo"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["homeLogo"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    
    if ($_FILES["homeLogo"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    
    } else {
        if (move_uploaded_file($_FILES["homeLogo"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["homeLogo"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
    $homeLogo = basename( $_FILES["homeLogo"]["name"]);
    
    $myfile = fopen("config.cfg", "w") or die("Unable to open file!");
    $txt = "_thisfile=config\n";
    fwrite($myfile, $txt);
    $txt = "setup=true\n";
    fwrite($myfile, $txt);
    $txt = "serveraddr=".$_POST['serveraddr']."\n";
    fwrite($myfile, $txt);
    $txt = "username=".$_POST['username']."\n";
    fwrite($myfile, $txt);
    $txt = "password=".$_POST['password']."\n";
    fwrite($myfile, $txt);
    $txt = "dbname=".$_POST['dbname']."\n";
    fwrite($myfile, $txt);
    $txt = "pageTitle=".$_POST['pageTitle']."\n";
    fwrite($myfile, $txt);
    $txt = "schoolTitle=".$_POST['schoolTitle']."\n";
    fwrite($myfile, $txt);
    $txt = "ger=".$_POST['ger']."\n";
    fwrite($myfile, $txt);
    if (isset($_POST['schoolTitleGer'])) {
        $txt = "schoolTitleGer=".$_POST['schoolTitleGer']."\n";
        fwrite($myfile, $txt);
    }
    $txt = "eng=".$_POST['eng']."\n";
    fwrite($myfile, $txt);
    if (isset($_POST['schoolTitleEng'])) {
        $txt = "schoolTitleEng=".$_POST['schoolTitleEng']."\n";
        fwrite($myfile, $txt);
    }
    $txt = "loginLogo=/res/".$loginLogo."\n";
    fwrite($myfile, $txt);
    $txt = "homeLogo=/res/".$homeLogo;
    fwrite($myfile, $txt);
    fclose($myfile);
    header("Location: /");
}

?>

<html>
    <head>
        <title>SzakdogaNgine telepítő</title>
    </head>
    <body>
        <form method="post" enctype="multipart/form-data">
            <fieldset>
               <legend>MySQL/MariaDB beállítása</legend>
                <b>Szerver cím </b><input type="text" name="serveraddr"><br>
                <b>Felhasználó név </b><input type="text" name="username"><br>
                <b>Jelszó </b><input type="text" name="password" placeholder="üresen hagyni, ha nincs"><br>
                <b>Adatbázis név </b><input type="text" name="dbname"><br>
            </fieldset>
            <fieldset>
               <legend>Testreszabás</legend>
                <b>Rövid iskola név (címsor) </b><input type="text" name="pageTitle"><br>
                <b>Iskola teljes neve </b><input type="text" name="schoolTitle"><br>
                <b>Bejelentkezési logó </b><input type="file" name="loginLogo" id="loginLogo"><br>
                <b>Home (bal felső) logó </b><input type="file" name="homeLogo" id="homeLogo"><br>
                <b>Német nyelv</b><br>
                <input type="radio" name="ger" onclick="changeGer()" value="false" checked> Nem<br>
                <input type="radio" name="ger" id="gerTrue" onclick="changeGer()" value="true"> Igen<br>
                <span id="gerInput"></span>
                <b>Angol nyelv</b><br>
                <input type="radio" name="eng" onclick="changeEng()" value="false" checked> Nem<br>
                <input type="radio" name="eng" id="engTrue" onclick="changeEng()" value="true"> Igen<br>
                <span id="engInput"></span>
            </fieldset>
            <fieldset><input type="submit" name="install" value="Install"></fieldset>
        </form>
    </body>
</html>
<script>
    function changeGer() {
        if (document.getElementById("gerTrue").checked == true) {
            document.getElementById("gerInput").innerHTML = "<b>Rövid iskola név (címsor) németül </b><input type='text' name='pageTitle'><br><b>Iskola teljes neve németül </b><input type='text' name='schoolTitleGer'><br>";
        }else{
            document.getElementById("gerInput").innerHTML = "";
        }
    }
    function changeEng() {
        if (document.getElementById("engTrue").checked == true) {
            document.getElementById("engInput").innerHTML = "<b>Rövid iskola név (címsor) angolul </b><input type='text' name='pageTitle'><br><b>Iskola teljes neve angolul </b><input type='text' name='schoolTitleEng'><br>";
        }else{
            document.getElementById("engInput").innerHTML = "";
        }
    }
</script>