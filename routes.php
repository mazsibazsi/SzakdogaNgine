<?php

function call($controller, $action) {
	require_once("controllers/" . $controller . "_controller.php");

	switch($controller) {
    case 'pages':
        $controller = new PagesController();
		break;
	case 'auth':
        $controller = new AuthController();
		break;
	}
	$controller->{ $action }();
}

$controllers = array(
					'home' => ['home', 'login', 'error'],
                    'pages' => ['home', 'login', 'error', 'mytopic', 'mytopics',
                                'studentlist', 'topiclist', 'consultantlist', 'deadlinelist', 'announcedtopics', 'messaging', 'manage', 'summary', 'protocol'],
					'auth'  => ['login', 'logout', 'error']
					);

if (array_key_exists($controller, $controllers)) {
  if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
  } else {
      call('pages', 'error');
  }
} else {
  call('pages', 'error');
}


?>