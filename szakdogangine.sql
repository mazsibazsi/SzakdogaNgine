-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2018. Feb 19. 22:37
-- Kiszolgáló verziója: 10.1.21-MariaDB
-- PHP verzió: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `szakdogangine`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `deadlines`
--

CREATE TABLE `deadlines` (
  `deadline_id` int(11) NOT NULL,
  `deadline_title` varchar(512) COLLATE utf8_hungarian_ci NOT NULL,
  `deadline_due` date NOT NULL,
  `deadline_desc` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `pms`
--

CREATE TABLE `pms` (
  `pm_id` int(11) UNSIGNED NOT NULL,
  `pm_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pm_recipient_user_id` int(11) DEFAULT NULL,
  `pm_sender_user_id` int(11) DEFAULT NULL,
  `pm_message` longtext CHARACTER SET utf8,
  `pm_unread` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `pms`
--

INSERT INTO `pms` (`pm_id`, `pm_time`, `pm_recipient_user_id`, `pm_sender_user_id`, `pm_message`, `pm_unread`) VALUES
(43, '2018-02-13 23:35:46', 10, 9, 'asd', 0),
(44, '2018-02-13 23:36:56', 9, 10, 'hdghdg', 0),
(45, '2018-02-13 23:37:12', 10, 9, 'asd', 0),
(46, '2018-02-13 23:38:35', 10, 9, 'fdasgsdfjhikgh', 0),
(47, '2018-02-13 23:38:46', 10, 9, 'fdas', 0),
(48, '2018-02-13 23:38:49', 10, 9, 'sdfh', 0),
(49, '2018-02-13 23:38:54', 9, 10, 'jnhgj', 0),
(50, '2018-02-13 23:38:55', 9, 10, 'jhfjfh', 0),
(51, '2018-02-13 23:39:00', 9, 10, 'vnvdbn', 0),
(53, '2018-02-13 23:39:26', 9, 10, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 0),
(54, '2018-02-13 23:40:27', 9, 10, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 0),
(55, '2018-02-13 23:40:52', 9, 10, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 0),
(56, '2018-02-13 23:40:59', 9, 10, 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 0),
(57, '2018-02-13 23:43:36', 10, 9, '\r\n                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.                        ', 0),
(58, '2018-02-13 23:43:47', 10, 9, '\r\n                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.                        ', 0),
(59, '2018-02-14 10:23:18', 9, 10, '12312\r\n', 0),
(60, '2018-02-14 10:23:27', 10, 9, 'asd', 0),
(61, '2018-02-14 10:26:45', 9, 10, 'asd', 0),
(62, '2018-02-16 12:52:06', 10, 11, 'jkltyuitiol', 0),
(63, '2018-02-16 17:49:58', 10, 9, 'hg', 0),
(64, '2018-02-18 23:12:30', 10, 32, 'zftg', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `protocols`
--

CREATE TABLE `protocols` (
  `protocol_student_user_id` int(11) NOT NULL,
  `protocol_final_title` varchar(256) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `protocol_1_pos` text COLLATE utf8_hungarian_ci,
  `protocol_1_mis` text COLLATE utf8_hungarian_ci,
  `protocol_1_rec` text COLLATE utf8_hungarian_ci,
  `protocol_1_prog_ready` int(3) DEFAULT NULL,
  `protocol_1_docu_ready` int(3) DEFAULT NULL,
  `protocol_2_pos` text COLLATE utf8_hungarian_ci,
  `protocol_2_mis` text COLLATE utf8_hungarian_ci,
  `protocol_2_rec` text COLLATE utf8_hungarian_ci,
  `protocol_2_prog_ready` int(3) DEFAULT NULL,
  `protocol_2_docu_ready` int(3) DEFAULT NULL,
  `protocol_3_pos` text COLLATE utf8_hungarian_ci,
  `protocol_3_mis` text COLLATE utf8_hungarian_ci,
  `protocol_3_rec` text COLLATE utf8_hungarian_ci,
  `protocol_3_prog_ready` int(3) DEFAULT NULL,
  `protocol_3_docu_ready` int(3) DEFAULT NULL,
  `protocol_exam_ready` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `protocols`
--

INSERT INTO `protocols` (`protocol_student_user_id`, `protocol_final_title`, `protocol_1_pos`, `protocol_1_mis`, `protocol_1_rec`, `protocol_1_prog_ready`, `protocol_1_docu_ready`, `protocol_2_pos`, `protocol_2_mis`, `protocol_2_rec`, `protocol_2_prog_ready`, `protocol_2_docu_ready`, `protocol_3_pos`, `protocol_3_mis`, `protocol_3_rec`, `protocol_3_prog_ready`, `protocol_3_docu_ready`, `protocol_exam_ready`) VALUES
(11, 'HexChess', 'Létezik', 'Az egész', 'Legyen kész', 17, 25, '', '', '', 0, 0, '', '', '', 0, 0, 0),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(32, 'vaéam,i', 'asd', 'asd', 'asd', 2, 2, '', '', '', 0, 0, '', '', '', 0, 0, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `students`
--

CREATE TABLE `students` (
  `student_user_id` int(11) NOT NULL,
  `student_class` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `student_topic_id` int(11) DEFAULT NULL,
  `student_upload` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `student_accept` tinyint(1) DEFAULT '0',
  `student_upload_2` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `student_cloud` varchar(256) COLLATE utf8_hungarian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `students`
--

INSERT INTO `students` (`student_user_id`, `student_class`, `student_topic_id`, `student_upload`, `student_accept`, `student_upload_2`, `student_cloud`) VALUES
(11, '2/14S', 5, 'uploads/tboRB3notGLIVCuBjZu8NiIVdHTVj689.odt', 1, NULL, NULL),
(12, '6/75J', 9, 'uploads/pPFqExonpn7ZW2PrDWfSvS7woiTYfTIo.docx', -1, NULL, NULL),
(21, '5/13T', 9, 'uploads/dF4UivRrFMXGpH6vx6LiJQ2biMEX9EPw.pdf', 1, NULL, NULL),
(23, '2/14S', NULL, NULL, 0, NULL, NULL),
(32, '5/13T', 1, 'uploads/UaeeQPmTd6w8Gn9l2SzGdg3t5fTTyKus.docx', 1, NULL, 'ztt');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `topics`
--

CREATE TABLE `topics` (
  `topic_id` int(11) NOT NULL,
  `topic_title` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `topic_recomm_tech` text CHARACTER SET utf8 NOT NULL,
  `topic_desc` longtext CHARACTER SET utf8,
  `topic_owner_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `topics`
--

INSERT INTO `topics` (`topic_id`, `topic_title`, `topic_recomm_tech`, `topic_desc`, `topic_owner_user_id`) VALUES
(1, 'Extrém Memory', 'C#,  Unity', 'A klasszikus Memory játék extrém kiadása, mindenféle ötletes nehezítésekkel, pl.  gravítáció, folyamatosan mozgó lapok, lapcsere stb.', 10),
(2, 'ex-Petrikes diákok pályakövetését lehetővé te', 'PHP/MySQL', 'Online kérdőívek elkészítését, kitöltését, kiértékelését megvalósító program. A kérdőíveket a Petrikben végzett diákok töltenék ki, és a válaszok az iskolavezetőség szempontjai alapján kerülnének kiértékelésre. Lényeges a kérdőívek egyszerű módosíthatósága. Regisztráció/jogosultságkezelés megvalósítása is szükséges.', 10),
(3, 'SchoolBook', 'PHP, MySQL, JavaScript', 'Webalkalmazás, amely lehetővé teszi egy, esetleg több osztály tanulói és tanárai, illetve a szülők közötti hatékony kommunikációt, az iskolai információk megosztását. Pl. osztály események, határidők, házi feladatok közzététele, tantárgyi fórumok, online fogadóóra, igazolás feltöltése, tanulói adatok frissítése stb.', 10),
(4, 'Játékos matematikai tudásellenőrző program', 'C#, Unity', 'A középiskolai matematika egy, vagy több fejezetét játékos formában számonkérő program. ', 10),
(5, 'Hexa sakk', 'C#, Unity', 'A sakkjáték 3 játékossal játszott változatának megvalósítása, hálózaton, MI nélkül, esetleg MI-vel.', 10),
(6, 'Tanárok értékelése webes alkalmazással', 'PHP, MySQL, JavaScript', 'Web alkalmazás, amely lehetővé teszi a tanév végén, esetleg közben is a tanárok oktatói munkájának a diákok általi értékelését, kérdőívek segítségével. A program támogatja a kérdőívek elkészítését, kitöltését, és kiértékelését.', 10),
(7, 'Záródolgozat témaválasztást és konzultációt segítő program', 'PHP, MySQL, JavaScript', 'Web alkalmazás, amely lehetővé teszi a záródolgozat témák tanárok általi kihirdetését, a diákok általi  kiválasztását, a specifikáció feltöltését/megtekintését/elfogadását, illetve az online konzultációt.', 10),
(8, 'Iskolai hibabejelentő program', 'C#, Java, PHP,  MySQL', 'Hálózati alkalmazás, amely lehetőséget ad a diákoknak, illetve tanároknak, hogy az iskolai gép- és eszközpark eszközeinek a meghibásodását be tudják jelenteni, illetve lehetővé teszi a hiba életútjának követését a vezetőség számára.', 10),
(9, 'Szabadon választható téma', 'HTML5, CSS, PHP &c.', '', 20);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(45) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_realname` varchar(127) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_email` varchar(128) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_pass` varchar(64) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_session_id` varchar(128) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `user_privilege` enum('admin','consultant','student') COLLATE utf8_hungarian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`user_id`, `user_username`, `user_realname`, `user_email`, `user_pass`, `user_session_id`, `user_privilege`) VALUES
(1, 'admin', 'Justan Adomin', 'admin@mazsibazsi.moe', '$2y$10$oOQYEd9QcXc3qcCQY6s4de9S0fK0hqpRUkdpk6Bgz37Qo7dVqBXo.', 'MzU2YTE5MmI3OTEzYjA0YzU0NTc0ZDE4YzI4ZDQ2ZTYzOTU0MjhhYjcyMTQ2YTViY2VhNWE0NTI2MzY4MWVkY2VhNGE1Mzc5YjAwOGQ0YmI=', 'admin'),
(10, 'krajnyak', 'Krajnyák Attila', 'krajnyakattila@petrik.hu', '$2y$10$MZOXgBwYxCSqENNjfMH6x.FkRxCTAhDakfLQTDSSice3Dsjih2oVy', 'YjFkNTc4MTExMWQ4NGY3YjNmZTQ1YTA4NTJlNTk3NThjZDdhODdlNTVhODI3YmUxNzI4MTE4NmJmMjI4YzQxZWMyYzlkMDcwMGFkZWJhMjg=', 'consultant'),
(11, '75611578000', 'Fábián Szilárd', 'fabioszillardo@gaymail.huh', '$2y$10$/UBamSzm1pYyJYDaSUbvUOiqG/ySsQZetOJNqJSIDBB9vNAeJzAo2', 'MTdiYTA3OTE0OTlkYjkwODQzM2I4MGYzN2M1ZmJjODliODcwMDg0YjM1N2Y4YTVlMjQxODI0OGNkZDRiNzdkNGE1OTBhMDFhNTVhMzA4ZjA=', 'student'),
(12, '72702180767', 'Sándor Máté', 'electropotato@gmail.com', '$2y$10$q/CwAmJ94o4Jpvv9d1LIhuOqvaIe1bVa6Qos5v8Ors61CuXIKXDGm', 'N2I1MjAwOWI2NGZkMGEyYTQ5ZTZkOGE5Mzk3NTMwNzc3OTJiMDU1NDA0ZjQ4OGExMzk1MTM5MjQwMzNlOGNkNDI4NDg2MGJlOTdlYzQ2OWE=', 'student'),
(15, NULL, '\0', NULL, '$2y$10$6toPj6shpohlBUVWlPZMN./jrrkuaBJP4v/tRXQeiZkHBu3x4pzKm', NULL, 'student'),
(17, NULL, '\0', NULL, '$2y$10$qBIICvBl5RS9rHhTq.QSDug6sc5NUOKTbUCWd2VK7QpeFKSV75jNy', NULL, 'student'),
(19, NULL, '\0', NULL, '$2y$10$XzTGm0Lk3txjNm3GkE58OOCjyFaBQkWSCxBaw.NM/99yXUrufqgfS', NULL, 'student'),
(20, 'ctamas', 'Czinkóczi Tamás', 'ctamas@petriktiszk.hu', '$2y$10$57OMDFG6rcU7ksoqvor6Qu0UEXUDYK..R6.OToWBd9BZpOu.6G0JO', 'OTEwMzJhZDdiYmNiNmNmNzI4NzVlOGU4MjA3ZGNmYmE4MDE3M2Y3YzY5YmZkZDE1N2U0YWE2NWQ5MDI1YjU2ZTZiM2VmZmY0NmI2MmNjMzA=', 'consultant'),
(21, '77147790309', 'Rigó Róbert', 'rigorobi@citromail.hu', '$2y$10$YskSmIvTRu33iUO9OiQ34upC6QJQupi6OJFsF6Yp.oUTu63VfUhoq', 'NDcyYjA3YjlmY2YyYzI0NTFlODc4MWU5NDRiZjVmNzdjZDg0NTdjODlkYmQwZWRiNDVmYzFlN2EyYjFmMjczZTllZjU1MmM2MDExNGRiYzk=', 'student'),
(23, '77326367833', 'Seemayer Péter', 'semy32@gmail.com', '$2y$10$e0yjYHxwOXqoQYMNjsYVoOQq1Oo9BUzm90tZ5BJOcJ7uqVP3A5FHu', NULL, 'student'),
(32, '72384709224', 'Máhli Balázs', 'mahli@posteo.net', '$2y$10$zdeikBiAv9w.Onk.WD0Nsu7JIxhenjgV4UjICC.66TS/.Ot60B0t6', 'Y2I0ZTUyMDhiNGNkODcyNjhiMjA4ZTQ5NDUyZWQ2ZTg5YTY4ZTBiODAzNjk3MDg4ZWNmYWE3MmM0MTc0YWEzNzM5YTM5OTJiNzMwNzc3NDM=', 'student');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `deadlines`
--
ALTER TABLE `deadlines`
  ADD PRIMARY KEY (`deadline_id`);

--
-- A tábla indexei `pms`
--
ALTER TABLE `pms`
  ADD PRIMARY KEY (`pm_id`);

--
-- A tábla indexei `protocols`
--
ALTER TABLE `protocols`
  ADD PRIMARY KEY (`protocol_student_user_id`);

--
-- A tábla indexei `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_user_id`),
  ADD KEY `fk_students_users_idx` (`student_user_id`),
  ADD KEY `fk_students_topics1` (`student_topic_id`);

--
-- A tábla indexei `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`topic_id`),
  ADD KEY `fk_topics_users1_idx` (`topic_owner_user_id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_username_UNIQUE` (`user_username`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `deadlines`
--
ALTER TABLE `deadlines`
  MODIFY `deadline_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `pms`
--
ALTER TABLE `pms`
  MODIFY `pm_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT a táblához `topics`
--
ALTER TABLE `topics`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `protocols`
--
ALTER TABLE `protocols`
  ADD CONSTRAINT `protocols_ibfk_1` FOREIGN KEY (`protocol_student_user_id`) REFERENCES `students` (`student_user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
